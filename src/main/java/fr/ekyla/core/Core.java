package fr.ekyla.core;

import com.google.common.io.Files;
import fr.ekyla.core.commands.InfoCommand;
import fr.ekyla.core.commands.MontureCommand;
import fr.ekyla.core.commands.NpcCommand;
import fr.ekyla.core.commands.RPRenameCommand;
import fr.ekyla.core.inventories.listener.InventoryListener;
import fr.ekyla.core.inventories.npc.BanquierInventory;
import fr.ekyla.core.inventories.npc.BucheronInventory;
import fr.ekyla.core.inventories.npc.CharpentierInventory;
import fr.ekyla.core.inventories.npc.MaquignonInventory;
import fr.ekyla.core.listener.WorldListener;
import fr.ekyla.core.npc.AbstractNpc;
import fr.ekyla.core.npc.listener.MontureListener;
import fr.ekyla.core.npc.listener.NpcListener;
import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.players.PlayerTask;
import fr.ekyla.core.players.factions.Faction;
import fr.ekyla.core.players.factions.FactionGson;
import fr.ekyla.core.players.listener.PlayerListener;
import fr.ekyla.core.quests.listener.QuestListener;
import fr.ekyla.core.spell.listener.SpellListener;
import fr.ekyla.core.sql.DatabaseAccess;
import fr.ekyla.core.sql.DatabaseCredentials;
import fr.ekyla.core.utils.JsonWriter;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Core extends JavaPlugin {

    private static DatabaseAccess mainDatabase;
    private static Core instance;
    private FactionGson factionGson = new FactionGson();

    public static DatabaseAccess getMainDatabase() {
        return mainDatabase;
    }

    public static Core getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        mainDatabase = new DatabaseAccess(new DatabaseCredentials("127.0.0.1", "ekyla", "password", "ekyla", 3306));
        mainDatabase.initPool();

        instance = this;
        saveDefaultConfig();
        loadNpc();
        registerEvents();
        registerCommands();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new PlayerTask(), 20L, 20L);
        registerFactions();
        registerInventories();
    }

    private void loadNpc() {
        File npcDir = new File(getDataFolder().getAbsolutePath() + "/npc/");
        int count = 0;
        if (!npcDir.exists()) npcDir.mkdir();
        for (File file : npcDir.listFiles()) {
            AbstractNpc npc = JsonWriter.getFromJson(file);
            if (npc == null) continue;
            npc.spawn();
            count++;
        }
        System.out.println("Loaded " + count + " npc.");
    }

    private void registerCommands() {
        getCommand("info").setExecutor(new InfoCommand());
        getCommand("npc").setExecutor(new NpcCommand());
        getCommand("rprename").setExecutor(new RPRenameCommand());
        getCommand("monture").setExecutor(new MontureCommand());
    }

    private void registerFactions() {
        /*try {
            final Connection connection = mainDatabase.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM factions");
            statement.executeQuery();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                Faction faction = new Faction(set.getInt("id"), set.getString("name"), set.getString("color"), set.getFloat("imposition"), set.getInt("faction_money"), set.getDouble("level"));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        File file = new File(getDataFolder(), "/factions/");
        if (!file.exists()) file.mkdir();
        for (File fac : file.listFiles()) {
            try {
                String content = Files.toString(fac, Charset.defaultCharset());
                factionGson.fromJson(content);
            } catch (IOException | IllegalStateException e) {
                System.out.println("Une erreur est survenue lors du chargement d'une faction (1002)");
            }
        }
        System.out.println("Loaded " + (Faction.getFactions().size() - 1) + " factions (and the Alone faction).");
    }

    private void saveFactions() {
        Faction.getFactions().forEach((faction -> {
            String json = factionGson.toJson(faction);
            File fac = new File(getDataFolder(), "/factions/" + faction.getName().toLowerCase() + ".json");
            try {
                FileUtils.writeStringToFile(fac, json, Charset.defaultCharset());
            } catch (IOException e) {
                System.out.println("Can't save the faction " + faction.getName());
            }
        }));
    }

    private void registerEvents() {
        registerEvent(new PlayerListener());
        registerEvent(new WorldListener());
        registerEvent(new InventoryListener());
        registerEvent(new NpcListener());
        registerEvent(new QuestListener());
        registerEvent(new MontureListener());
        registerEvent(new SpellListener());
    }

    private void registerInventories() {
        new BucheronInventory();
        new BanquierInventory();
        new CharpentierInventory();
        new MaquignonInventory();
    }

    private void registerEvent(Listener listener) {
        getServer().getPluginManager().registerEvents(listener, this);
    }

    @Override
    public void onDisable() {
        EkylaPlayer.getEkylaPlayers().forEach((player -> {
            if (player.getPlayer().isOnline()) player.getPlayer().kickPlayer("§cVous avez été deconnecté");
            player.pushToDatabase();
        }));
        try {
            final Connection connection = mainDatabase.getConnection();
            for (Faction faction : Faction.getFactions()) {
                PreparedStatement statement = connection.prepareStatement("UPDATE factions SET imposition=" + faction.getImposition() + ", faction_money=" + faction.getMoney() + ", level=" + faction.getLevel() + " WHERE id=" + faction.getId());
                statement.executeUpdate();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        AbstractNpc.stopNpc();
        saveFactions();
        mainDatabase.closePool();
    }
}
