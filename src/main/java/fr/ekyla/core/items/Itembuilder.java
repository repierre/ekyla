package fr.ekyla.core.items;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.Map;

/**
 * @author Feedthecookie
 */
public class Itembuilder {

    private Material material;
    private int amount = 1;
    private byte data;
    private String name;
    private boolean glow = false;
    private List<String> lore = Lists.newArrayList();
    private Map<Enchantment, Integer> enchants = Maps.newHashMap();

    public Itembuilder(Material material) {
        this.material = material;
    }

    public static Itembuilder fromItemStack(ItemStack stack) {
        Validate.notNull(stack, "ItemStack can't be null");
        ItemMeta meta = stack.getItemMeta();
        return new Itembuilder(stack.getType()).setAmount(stack.getAmount()).setData(stack.getDurability())
                .setName(meta.getDisplayName()).setLore(meta.getLore())
                .setGlowing(meta.hasItemFlag(ItemFlag.HIDE_ENCHANTS));
    }

    public Itembuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public Itembuilder setData(int data) {
        this.data = (byte) data;
        return this;
    }

    public Itembuilder setName(String name) {
        this.name = name;
        return this;
    }

    public Itembuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public Itembuilder addEnchantment(Enchantment enchant, int level) {
        this.enchants.put(enchant, level);
        return this;
    }

    public Itembuilder setEnchantments(Map<Enchantment, Integer> enchants) {
        this.enchants = enchants;
        return this;
    }

    public boolean isGlowing() {
        return this.glow;
    }

    public Itembuilder setGlowing(boolean glow) {
        this.glow = glow;
        return this;
    }

    public ItemStack buildItemStack() {
        Validate.notNull(this.material, "Material cannot be null");
        ItemStack stack = new ItemStack(this.material, this.amount, this.data);
        ItemMeta meta = stack.getItemMeta();
        if (glow)
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        if (this.name != null)
            meta.setDisplayName(this.name);
        if (this.lore != null && !this.lore.isEmpty())
            meta.setLore(this.lore);
        if (!this.enchants.isEmpty())
            stack.addUnsafeEnchantments(this.enchants);
        stack.setItemMeta(meta);
        if (this.glow) {
            net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
            if (nmsStack == null)
                return stack;
            NBTTagCompound tag = null;
            if (!nmsStack.hasTag()) {
                tag = new NBTTagCompound();
                nmsStack.setTag(tag);
            }
            if (tag == null)
                tag = nmsStack.getTag();
            NBTTagList ench = new NBTTagList();
            tag.set("ench", ench);
            stack = CraftItemStack.asBukkitCopy(nmsStack);
        }
        return stack;
    }

}
