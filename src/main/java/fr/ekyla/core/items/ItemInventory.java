package fr.ekyla.core.items;

import org.bukkit.Material;

/**
 * File <b>ItemInventory</b> located on fr.ekyla.core.items
 * ItemInventory is a part of ekyla.
 * <p>
 * Copyright (c) 2017 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 03/03/2018 at 20:21
 */
public class ItemInventory extends Itembuilder {

    private int position;

    public ItemInventory(Material material, int position) {
        super(material);
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
