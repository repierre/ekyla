package fr.ekyla.core.inventories.npc;

import fr.ekyla.core.inventories.AbstractInventory;
import fr.ekyla.core.npc.messages.MaquignonMessages;
import fr.ekyla.core.npc.trade.MaquignonTrades;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class MaquignonInventory extends AbstractInventory {

    public MaquignonInventory() {
        super("§8Maquignon");
    }

    @Override
    public void open(EkylaPlayer player) {
        Inventory inventory = Bukkit.createInventory(null, 9 * 6, getTitle());
        player.getPlayer().openInventory(inventory);
        MaquignonTrades.setMaquignonItems(player);
    }

    @Override
    public void onClick(EkylaPlayer player, ItemStack itemStack) {
        List<String> lore = itemStack.getItemMeta().getLore();
        String name = itemStack.getItemMeta().getDisplayName();

        if (lore.get(0).startsWith("§4Acheter: §e")) {
            double price = Double.valueOf(lore.get(0).substring(13).substring(0, lore.get(0).length() - 16));
            if (player.hasMoney(price)) {
                player.sendMessage("§8Maquignon §7»", MaquignonMessages.getMaquignonRandomMessage(MaquignonMessages.buysucess).replaceAll("%price", price + ""));
                player.playSound(Sound.ENTITY_HORSE_AMBIENT, 1, 1);
                player.removeMoney(price);

                if (name.startsWith("§6Couleur : Blanc")) {
                    player.getMontureData().setColorWHITE(1);
                    player.getMontures().setColor("WHITE");
                }
                if (name.startsWith("§6Couleur : Crémeux")) {
                    player.getMontureData().setColorCREAMY(1);
                    player.getMontures().setColor("CREAMY");
                }
                if (name.startsWith("§6Couleur : Noisette")) {
                    player.getMontureData().setColorCHESTNUT(1);
                    player.getMontures().setColor("CHESTNUT");
                }
                if (name.startsWith("§6Couleur : Marron Clair")) {
                    player.getMontureData().setColorBROWN(1);
                    player.getMontures().setColor("BROWN");
                }
                if (name.startsWith("§6Couleur : Noir")) {
                    player.getMontureData().setColorBLACK(1);
                    player.getMontures().setColor("BLACK");
                }
                if (name.startsWith("§6Couleur : Gris")) {
                    player.getMontureData().setColorGRAY(1);
                    player.getMontures().setColor("GRAY");
                }
                if (name.startsWith("§6Couleur : Marron Foncé")) {
                    player.getMontureData().setColorDARK_BROWN(1);
                    player.getMontures().setColor("DARK_BROWN");
                }

                if (name.startsWith("§6Style : Aucun")) {
                    player.getMontureData().setStyleNONE(1);
                    player.getMontures().setStyle("NONE");
                }
                if (name.startsWith("§6Style : Blanc")) {
                    player.getMontureData().setStyleWHITE(1);
                    player.getMontures().setStyle("WHITE");
                }
                if (name.startsWith("§6Style : Taches Blanches")) {
                    player.getMontureData().setStyleWHITEFIELD(1);
                    player.getMontures().setStyle("WHITEFIELD");
                }
                if (name.startsWith("§6Style : Points Blancs")) {
                    player.getMontureData().setStyleWHITE_DOTS(1);
                    player.getMontures().setStyle("WHITE_DOTS");
                }
                if (name.startsWith("§6Style : Points Noirs")) {
                    player.getMontureData().setStyleBLACK_DOTS(1);
                    player.getMontures().setStyle("BLACK_DOTS");
                }

                if (name.startsWith("§6Armure : Fer")) {
                    player.getMontureData().setArmorIRON_BARDING(1);
                    player.getMontures().setArmor("IRON_BARDING");
                }
                if (name.startsWith("§6Armure : Or")) {
                    player.getMontureData().setArmorGOLD_BARDING(1);
                    player.getMontures().setArmor("GOLD_BARDING");
                }
                if (name.startsWith("§6Armure : Diamant")) {
                    player.getMontureData().setArmorDIAMOND_BARDING(1);
                    player.getMontures().setArmor("DIAMOND_BARDING");
                }

                if (name.startsWith("§6Vitesse : 120%")) {
                    player.getMontures().setSpeed(0.3F);
                }
                if (name.startsWith("§6Vitesse : 140%")) {
                    player.getMontures().setSpeed(0.35F);
                }
                if (name.startsWith("§6Vitesse : 160%")) {
                    player.getMontures().setSpeed(0.4F);
                }

                if (name.startsWith("§6Saut : 120%")) {
                    player.getMontures().setJump(0.6F);
                }
                if (name.startsWith("§6Saut : 140%")) {
                    player.getMontures().setJump(0.7F);
                }
                if (name.startsWith("§6Saut : 160%")) {
                    player.getMontures().setJump(0.8F);
                }

                if (name.startsWith("§6Vie : 120%")) {
                    player.getMontures().setHealth(24F);
                }
                if (name.startsWith("§6Vie : 140%")) {
                    player.getMontures().setHealth(28F);
                }
                if (name.startsWith("§6Vie : 160%")) {
                    player.getMontures().setHealth(32F);
                }


                MaquignonTrades.setMaquignonItems(player);
            } else {
                player.sendMessage("§8Maquignon §7»", MaquignonMessages.getMaquignonRandomMessage(MaquignonMessages.buyfail).replaceAll("%money", player.getMoney() + "").replaceAll("%pname", player.getRPName() + ""));
            }
        }

        if (lore.get(0).startsWith("§2Appliquer")) {
            player.playSound(Sound.ENTITY_HORSE_AMBIENT, 1, 1);

            if (name.startsWith("§6Couleur : Blanc")) {
                player.getMontures().setColor("WHITE");
            }
            if (name.startsWith("§6Couleur : Crémeux")) {
                player.getMontures().setColor("CREAMY");
            }
            if (name.startsWith("§6Couleur : Noisette")) {
                player.getMontures().setColor("CHESTNUT");
            }
            if (name.startsWith("§6Couleur : Marron Clair")) {
                player.getMontures().setColor("BROWN");
            }
            if (name.startsWith("§6Couleur : Noir")) {
                player.getMontures().setColor("BLACK");
            }
            if (name.startsWith("§6Couleur : Gris")) {
                player.getMontures().setColor("GRAY");
            }
            if (name.startsWith("§6Couleur : Marron Foncé")) {
                player.getMontures().setColor("DARK_BROWN");
            }

            if (name.startsWith("§6Style : Aucun")) {
                player.getMontures().setStyle("NONE");
            }
            if (name.startsWith("§6Style : Blanc")) {
                player.getMontures().setStyle("WHITE");
            }
            if (name.startsWith("§6Style : Taches Blanches")) {
                player.getMontures().setStyle("WHITEFIELD");
            }
            if (name.startsWith("§6Style : Points Blancs")) {
                player.getMontures().setStyle("WHITE_DOTS");
            }
            if (name.startsWith("§6Style : Points Noirs")) {
                player.getMontures().setStyle("BLACK_DOTS");
            }

            if (name.startsWith("§6Armure : Fer")) {
                player.getMontures().setArmor("IRON_BARDING");
            }
            if (name.startsWith("§6Armure : Or")) {
                player.getMontures().setArmor("GOLD_BARDING");
            }
            if (name.startsWith("§6Armure : Diamant")) {
                player.getMontures().setArmor("DIAMOND_BARDING");
            }

            if (name.startsWith("§6Monture : Cheval")) {
                player.getMontures().setType("Horses");
            }
            if (name.startsWith("§6Monture : Âne")) {
                player.getMontures().setType("Donkeys");
            }
            if (name.startsWith("§6Monture : Mule")) {
                player.getMontures().setType("Mules");
            }
            if (name.startsWith("§6Monture : Roach")) {
                player.getMontures().setType("Roach");
            }

            MaquignonTrades.setMaquignonItems(player);
        }
    }
}
