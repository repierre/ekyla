package fr.ekyla.core.inventories.npc;

import fr.ekyla.core.inventories.AbstractInventory;
import fr.ekyla.core.items.Itembuilder;
import fr.ekyla.core.npc.messages.CharpentierMessages;
import fr.ekyla.core.npc.trade.CharpentierTrades;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CharpentierInventory extends AbstractInventory {

    public CharpentierInventory() {
        super("§2§0Charpentier");
    }

    @Override
    public void open(EkylaPlayer player) {
        Inventory inventory = Bukkit.createInventory(null, 9 * 6, getTitle());
        player.getPlayer().openInventory(inventory);
        CharpentierTrades.setCharpentierAchatItems(player);
    }

    @Override
    public void onClick(EkylaPlayer player, ItemStack itemStack) {
        List<String> lore = itemStack.getItemMeta().getLore();
        String name = itemStack.getItemMeta().getDisplayName();

        if (lore.get(0).startsWith("§6Acheter: §e")) {
            double price = Double.valueOf(lore.get(0).substring(13).substring(0, lore.get(0).length() - 16));
            if (player.hasMoney(price)) {
                player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.buysucess).replaceAll("%price", price + ""));
                player.playSound(Sound.BLOCK_ANVIL_USE, 1, 1);
                player.removeMoney(price);
                player.getPlayer().getInventory().addItem(new Itembuilder(itemStack.getType()).setData(itemStack.getData().getData()).setName(itemStack.getItemMeta().getDisplayName()).buildItemStack());
                CharpentierTrades.setCharpentierAchatItems(player);
            } else {
                player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.buyfail).replaceAll("%money", player.getMoney() + "").replaceAll("%pname", player.getRPName() + ""));
            }
        }

        if (lore.get(0).startsWith("§6Vendre: §e")) {
            double price = Double.valueOf(lore.get(0).substring(12).substring(0, lore.get(0).length() - 15));
            int nbitem = 0;
            for (ItemStack pItem : player.getPlayer().getInventory().getContents()) {
                if (pItem != null) {
                    if (pItem.getType() == itemStack.getType()) {
                        if (pItem.getData().getData() == itemStack.getData().getData()) {
                            player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.sellsucess).replaceAll("%price", price + ""));
                            player.playSound(Sound.BLOCK_ANVIL_USE, 1, 1);
                            player.addMoney(price);
                            nbitem = pItem.getAmount();
                            pItem.setAmount(nbitem - 1);
                            CharpentierTrades.setCharpentierVenteItems(player);
                            nbitem = -1;
                            break;
                        }
                    }
                }
            }
            if (nbitem != -1) {
                player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.sellfail).replaceAll("%pname", player.getRPName() + ""));
            }
        }

        if (lore.get(0).startsWith("§6Contenu de votre bourse:")) {
            if (name.startsWith("§6Passer en mode Vente")) CharpentierTrades.setCharpentierVenteItems(player);
            else CharpentierTrades.setCharpentierAchatItems(player);
        }
    }
}
