package fr.ekyla.core.inventories.npc;

import fr.ekyla.core.Core;
import fr.ekyla.core.inventories.AbstractInventory;
import fr.ekyla.core.items.Itembuilder;
import fr.ekyla.core.npc.messages.BanquierMessages;
import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.players.bank.BankAccount;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;

public class BanquierInventory extends AbstractInventory {

    public BanquierInventory() {
        super("§6Banquier");
    }

    private void setItems(Inventory inventory, EkylaPlayer player) {
        Itembuilder add = new Itembuilder(Material.WOOL).setData(13);
        Itembuilder get = new Itembuilder(Material.WOOL).setData(14);
        inventory.setItem(10, get.setName("§cDéposer toutes vos §ePO").setLore(Collections.singletonList("§cDéposer: §e" + player.getMoney())).buildItemStack());
        inventory.setItem(3, get.setName("§cDéposer §e1 PO").setLore(Collections.singletonList("§cDéposer: §e1")).buildItemStack());
        inventory.setItem(2, get.setName("§cDéposer §e5 PO").setLore(Collections.singletonList("§cDéposer: §e5")).buildItemStack());
        inventory.setItem(12, get.setName("§cDéposer §e10 PO").setLore(Collections.singletonList("§cDéposer: §e10")).buildItemStack());
        inventory.setItem(11, get.setName("§cDéposer §e50 PO").setLore(Collections.singletonList("§cDéposer: §e50")).buildItemStack());
        inventory.setItem(21, get.setName("§cDéposer §e100 PO").setLore(Collections.singletonList("§cDéposer: §e100")).buildItemStack());
        inventory.setItem(20, get.setName("§cDéposer §e500 PO").setLore(Collections.singletonList("§cDéposer: §e500")).buildItemStack());

        inventory.setItem(13, new Itembuilder(Material.WOOL).setData(4).setName("§6Votre compte").setLore(Arrays.asList("§6 - Titulaire du compte: " + player.getRPName(), "§6 - Somme présente: §e" + player.getBankAccount().getMoney() + " PO", "§6 - Taux d'imposition: §c" + player.getBankAccount().getPlayerImpisition() + "%")).buildItemStack());

        inventory.setItem(16, add.setName("§aRetirer toutes vos §ePO").setLore(Collections.singletonList("§aRetirer: §e" + player.getBankAccount().getMoney())).buildItemStack());
        inventory.setItem(5, add.setName("§aRetirer §e1 PO").setLore(Collections.singletonList("§aRetirer: §e1")).buildItemStack());
        inventory.setItem(6, add.setName("§aRetirer §e5 PO").setLore(Collections.singletonList("§aRetirer: §e5")).buildItemStack());
        inventory.setItem(14, add.setName("§aRetirer §e10 PO").setLore(Collections.singletonList("§aRetirer: §e10")).buildItemStack());
        inventory.setItem(15, add.setName("§aRetirer §e50 PO").setLore(Collections.singletonList("§aRetirer: §e50")).buildItemStack());
        inventory.setItem(23, add.setName("§aRetirer §e100 PO").setLore(Collections.singletonList("§aRetirer: §e100")).buildItemStack());
        inventory.setItem(24, add.setName("§aRetirer §e500 PO").setLore(Collections.singletonList("§aRetirer: §e500")).buildItemStack());
    }

    @Override
    public void open(EkylaPlayer player) {
        if (player.hasAccount()) {
            Inventory inventory = Bukkit.createInventory(null, 9 * 3, getTitle());
            setItems(inventory, player);
            player.getPlayer().openInventory(inventory);
        } else {
            if (player.getFactionId() == 1) {
                player.sendMessage("§6Banquier §7»", "Hum... D'après mes fiches vous ne disposez pas des autrisations nécessaires pour pouvoir créer votre compte.");
                return;
            } else {
                player.sendMessage("§6Banquier §7»", "Bonjour, votre nom s'il vous plaît.");
                player.say(7, "Je suis " + player.getRPName() + ".", 20);
                player.sendMessage("§6Banquier §7»", "Très bien alors allons-y, un nouveau compte pour Mr. " + player.getRPName(), 40);
                player.sendMessage("§6Banquier §7»", "Et voilà, votre nouveau compte est prêt.", 50);
                player.setBankAccount(new BankAccount(player, 0, 0, player.getFaction()));
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    open(player);
                }, 60L);
            }

            try {
                final Connection connection = Core.getMainDatabase().getConnection();
                PreparedStatement statement = connection.prepareStatement("INSERT INTO bank_account (uuid, faction_id, money, imposition) VALUES ('" + player.getUuid().toString() + "', " + player.getFactionId() + ", 0, 0)");
                statement.executeUpdate();

                connection.close();
            } catch (SQLException e) {
                player.sendMessage("Une erreur est survzenue lors de la création de votre compte. (0003)");
            }
        }
    }

    @Override
    public void onClick(EkylaPlayer player, ItemStack itemStack) {
        String type = itemStack.getItemMeta().getDisplayName();
        if (type.startsWith("§cDéposer")) {
            double amount = Double.valueOf(itemStack.getItemMeta().getLore().get(0).substring(13));
            if (player.hasMoney(amount) && player.getMoney() != 0) {
                player.getBankAccount().addMoney(amount);
                player.removeMoney(amount);
                player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.depossucces).replaceAll("%amount", amount + ""));
                setItems(player.getPlayer().getOpenInventory().getTopInventory(), player);
            } else {
                if (player.getMoney() != 0) {
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.deposfailrest).replaceAll("%amount", amount + ""));
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.deposrest).replaceAll("%money", player.getMoney() + ""));
                    player.getBankAccount().addMoney(player.getMoney());
                    player.removeMoney(player.getMoney());
                    setItems(player.getPlayer().getOpenInventory().getTopInventory(), player);
                } else {
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.deposfail));
                    setItems(player.getPlayer().getOpenInventory().getTopInventory(), player);
                }
            }

        } else if (type.startsWith("§aRetirer")) {
            double amount = Double.valueOf(itemStack.getItemMeta().getLore().get(0).substring(13));
            if (player.getBankAccount().hasMoney(amount) && player.getBankAccount().getMoney() != 0) {
                player.getBankAccount().removeMoney(amount);
                player.addMoney(amount);
                player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.retraitsucces).replaceAll("%amount", amount + ""));
                setItems(player.getPlayer().getOpenInventory().getTopInventory(), player);
            } else {
                if (player.getBankAccount().getMoney() != 0) {
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.retraitfailrest).replaceAll("%amount", amount + ""));
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.retraitrest).replaceAll("%bank", player.getBankAccount().getMoney() + ""));
                    player.getBankAccount().removeMoney(player.getBankAccount().getMoney());
                    player.addMoney(player.getBankAccount().getMoney());
                    setItems(player.getPlayer().getOpenInventory().getTopInventory(), player);
                } else {
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.retraitfail));
                    setItems(player.getPlayer().getOpenInventory().getTopInventory(), player);
                }
            }
        }
    }
}
