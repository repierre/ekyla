package fr.ekyla.core.inventories;

import fr.ekyla.core.items.ItemInventory;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

/**
 * File <b>Inventory</b> located on fr.ekyla.core.inventories
 * Inventory is a part of ekyla.
 * <p>
 * Copyright (c) 2017 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 04/03/2018 at 22:38
 */
public class Inventory {

    private List<ItemInventory> items = new ArrayList<>();
    private String name;
    private int slots;
    private int maxSlots;

    /**
     * Create an inventory with the name and the slots (mutliple of 9)
     *
     * @param name  the name
     * @param slots the number of slots (multiple of 9)
     */
    public Inventory(String name, int slots) {
        this.name = name;
        this.slots = slots;
        this.maxSlots = slots;
    }

    /**
     * Create an inventory and the slots (mutliple of 9)
     *
     * @param slots the number of slots (multiple of 9)
     */
    public Inventory(int slots) {
        this.slots = slots;
        this.name = "";
        this.maxSlots = slots;
    }

    /**
     * Create an inventory with the name and the slots (mutliple of 9) and the max slots
     *
     * @param name     the name
     * @param slots    the number of slots (multiple of 9)
     * @param maxSlots the maximum of slot utilisable
     */
    public Inventory(String name, int slots, int maxSlots) {
        this.name = name;
        this.slots = slots;
        this.maxSlots = maxSlots;
    }

    /**
     * Add an item to the inventory
     *
     * @param item
     */
    public void addItem(ItemInventory item) {
        items.add(item);
    }

    /**
     * @return
     */
    public List<ItemInventory> getItems() {
        return items;
    }

    public void setItems(List<ItemInventory> items) {
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public int getMaxSlots() {
        return maxSlots;
    }

    public void setMaxSlots(int maxSlots) {
        this.maxSlots = maxSlots;
    }

    void t() {
        org.bukkit.inventory.Inventory inv = Bukkit.createInventory(null, slots, name);
        items.forEach((item) -> inv.setItem(item.getPosition(), item.buildItemStack()));

    }

}
