package fr.ekyla.core.inventories;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractInventory implements Listener {

    private static List<AbstractInventory> inventories = new ArrayList<>();
    private String title;

    public AbstractInventory(String title) {
        this.title = title;
        inventories.add(this);
    }

    public static List<AbstractInventory> getInventories() {
        return inventories;
    }

    public static AbstractInventory getInventory(String title) {
        for (AbstractInventory inventory : getInventories())
            if (inventory.getTitle().startsWith(title)) return inventory;
        return null;
    }

    public abstract void open(EkylaPlayer player);

    public abstract void onClick(EkylaPlayer player, ItemStack itemStack);

    public String getTitle() {
        return title;
    }

}
