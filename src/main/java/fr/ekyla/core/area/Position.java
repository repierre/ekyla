package fr.ekyla.core.area;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * File <b>Position</b> located on fr.ekyla.core.players
 * Position is a part of ekyla.
 * <p>
 * Copyright (c) 2017 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 07/03/2018 at 17:10
 */
public class Position {

    private World world = Bukkit.getWorld("world");
    private double x;
    private double y;
    private double z;
    private float yaw = 0;
    private float pitch = 0;

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position(double x, double y, double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public Position(World world, double x, double y, double z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position(World world, double x, double y, double z, float yaw, float pitch) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public World getWorld() {
        return world;
    }

    public Position setWorld(World world) {
        this.world = world;
        return this;
    }

    public double getX() {
        return x;
    }

    public Position setX(double x) {
        this.x = x;
        return this;
    }

    public double getY() {
        return y;
    }

    public Position setY(double y) {
        this.y = y;
        return this;
    }

    public double getZ() {
        return z;
    }

    public Position setZ(double z) {
        this.z = z;
        return this;
    }

    public float getYaw() {
        return yaw;
    }

    public Position setYaw(float yaw) {
        this.yaw = yaw;
        return this;
    }

    public float getPitch() {
        return pitch;
    }

    public Position setPitch(float pitch) {
        this.pitch = pitch;
        return this;
    }

    public Location toLocation() {
        return new Location(world, x, y, z, yaw, pitch);
    }

    public Position addZ(double i) {
        this.z += i;
        return this;
    }

    public Position addX(double i) {
        this.x += i;
        return this;
    }

    public Position addY(double i) {
        this.y += i;
        return this;
    }

    @Override
    public String toString() {
        return "Position{" +
                "world=" + world +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", yaw=" + yaw +
                ", pitch=" + pitch +
                '}';
    }
}
