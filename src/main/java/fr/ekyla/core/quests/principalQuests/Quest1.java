package fr.ekyla.core.quests.principalQuests;

import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.players.chat.Message;
import fr.ekyla.core.players.locations.Locations;
import fr.ekyla.core.players.npc.NPC;
import fr.ekyla.core.quests.AQuest;
import fr.ekyla.core.quests.Callback;
import fr.ekyla.core.quests.steps.AQuestStep;
import fr.ekyla.core.quests.steps.ListenStep;
import net.minecraft.server.v1_12_R1.EntityLiving;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.List;

public class Quest1 extends AQuest {

    public Quest1(EkylaPlayer player) {
        super(1, "Premier réveil", player.getQuestStep(), player);
    }

    @Override
    protected List<AQuestStep> registerSteps() {
        return Arrays.asList(new ListenToMedic());
    }

    @Override
    protected void onStart() {
        getPlayer().getLocationUtil().teleport(Locations.HOSPITAL);
    }

    @Override
    protected void onFinish() {

    }

    private class ListenToMedic extends ListenStep {

        public ListenToMedic() {
            super(1, "Écouter le médecin", getObject(), Locations.HOSPITAL.toPosition().addY(-0.5).setYaw(-180).setPitch(0).addZ(2));
        }

        @Override
        protected void onStart() {
            EntityLiving entity = getPlayer().summonNpc(NPC.MEDECIN, getRange());
            getPlayer().getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 5 * 20, 1));
            getPlayer().getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5 * 20, 1));
            startChat(
                    Arrays.asList(
                            new Message("§dMédecin §7» Ho ! Vous êtes réveillé !", 0, null),
                            new Message("§dMédecin §7» Un homme vous a trouvé", 20, null),
                            new Message("§dMédecin §7» Mais, vous souvenez vous de votre nom ?", 10, new Callback<String>() {
                                @Override
                                public void done(String s) {
                                    getPlayer().setRPName(s);
                                    nextMessage();
                                }
                            }),
                            new Message("§dMédecin §7» §6%name% §7! Excellent !", 20, null)
                    ), new Callback<EkylaPlayer>() {
                        @Override
                        public void done(EkylaPlayer player) {
                            player.removeNpc(entity.getId());
                            finish();
                        }
                    });
        }

        @Override
        protected void onFinish() {

        }

    }

}
