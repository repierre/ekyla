package fr.ekyla.core.quests.listener;

import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.quests.AQuest;
import fr.ekyla.core.quests.steps.ListenStep;
import fr.ekyla.core.quests.steps.MoveStep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class QuestListener implements Listener {

    @EventHandler
    public void move(PlayerMoveEvent event) {
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        if (player.getQuestStep() != 0 && player.getQuest() != null) {
            AQuest quest = player.getQuest();
            if (quest.getCurrentStep() instanceof MoveStep) {
                MoveStep moveStep = (MoveStep) quest.getCurrentStep();
                moveStep.move(event);
                if (player.getPlayer().getLocation().distanceSquared(moveStep.getLocationTo()) <= moveStep.getRadius()) {
                    moveStep.arrive();
                }
            } else if (quest.getCurrentStep() instanceof ListenStep) {
            }
        }
    }

}
