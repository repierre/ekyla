package fr.ekyla.core.quests;

public abstract class Callback<T> {

    public abstract void done(T t);

}
