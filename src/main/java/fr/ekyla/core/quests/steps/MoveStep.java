package fr.ekyla.core.quests.steps;

import fr.ekyla.core.quests.AQuest;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;

public abstract class MoveStep extends AQuestStep {

    private Location locationTo;
    private double radius;

    public MoveStep(int id, String name, AQuest mainQuest, Location locationTo, double radius) {
        super(id, name, mainQuest);
        this.locationTo = locationTo;
        this.radius = radius;
    }

    public Location getLocationTo() {
        return locationTo;
    }

    public double getRadius() {
        return radius;
    }

    protected abstract void onArrive();

    protected abstract void onMove(PlayerMoveEvent event);

    public final void arrive() {
        onArrive();
        finish();
    }

    public final void move(PlayerMoveEvent event) {
        onMove(event);
    }


}
