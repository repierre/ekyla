package fr.ekyla.core.quests.steps;

import fr.ekyla.core.area.Position;
import fr.ekyla.core.quests.AQuest;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;

public abstract class ListenStep extends AQuestStep {

    private Position range;
    private Map<String, Long> messages = new HashMap<>();

    public ListenStep(int id, String name, AQuest mainQuest, Position range) {
        super(id, name, mainQuest);
        this.range = range;
    }

    public Position getRange() {
        return range;
    }

    public Map<String, Long> getMessages() {
        return messages;
    }

    public void addMessage(String message, long delay) {
        messages.put(message, delay);
    }

}
