package fr.ekyla.core.players.chat;

import fr.ekyla.core.Core;
import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.quests.Callback;
import org.bukkit.Bukkit;

import java.util.List;

public class RPChat {

    private EkylaPlayer player;
    private List<Message> messages;
    private Callback<EkylaPlayer> callback;

    private int nb = 0;
    private Message currentMessage;

    public RPChat(EkylaPlayer player, List<Message> messages, Callback<EkylaPlayer> callback) {
        this.player = player;
        this.messages = messages;
        this.callback = callback;
    }

    public RPChat(EkylaPlayer player, List<Message> messages) {
        this.player = player;
        this.messages = messages;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public RPChat nextMessage() {
        if (nb == messages.size()) {
            callback.done(player);
            return null;
        }
        this.currentMessage = messages.get(nb);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
            player.sendMessage(currentMessage.getMessage()
                    .replaceAll("%name%", player.getRPName())
            );
            if (currentMessage.getResponse() == null) nextMessage();
        }, currentMessage.getDelay());
        nb++;
        return this;
    }

    public Message getCurrentMessage() {
        return currentMessage;
    }
}
