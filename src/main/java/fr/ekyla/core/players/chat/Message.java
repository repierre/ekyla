package fr.ekyla.core.players.chat;

import fr.ekyla.core.quests.Callback;

public class Message {

    private String message;
    private long delay;
    private Callback<String> response;

    public Message(String message, long delay, Callback<String> response) {
        this.message = message;
        this.delay = delay;
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public long getDelay() {
        return delay;
    }

    public Callback<String> getResponse() {
        return response;
    }
}
