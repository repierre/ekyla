package fr.ekyla.core.players.listener;

import fr.ekyla.core.Core;
import fr.ekyla.core.chat.ChatUtils;
import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.players.PlayerUtils;
import fr.ekyla.core.players.factions.FactionRoles;
import fr.ekyla.core.utils.BroadcastUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.server.ServerListPingEvent;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.util.Random;

public class PlayerListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        EkylaPlayer player = PlayerUtils.getAccount(event.getPlayer());
        if (player.getAccountConfigured() != 1) {
            player.sendMessage("§6Petite voix §7»", "§7§oVotre compte n'est pas configuré veuillez entrer un nom RP !");
            player.getPlayer().setWalkSpeed(0F);
        }
        //Messages
        event.setJoinMessage(null);
        if (player.hasPermission("server.connection.message")) {
            BroadcastUtils.broadcastMessage(player.getRank().getPrefix() + player.getUsername() + " §aà rejoint le serveur");
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        player.disconnect();
    }

    @EventHandler
    public void onPing(ServerListPingEvent event) {
        StringBuilder builder = new StringBuilder();
        builder.append(ChatUtils.getCenteredText("&8| &6&lEkyla - MMORpg &8|")).append("\n").append(ChatUtils.getCenteredText("&4» &4Une maintenance est en cours &4«"));
        event.setMotd(ChatColor.translateAlternateColorCodes('&', builder.toString()));
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        if (message.startsWith("/")) return;
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        event.setCancelled(true);
        if (player.getAccountConfigured() != 1) {
            if (message.length() > 32) {
                player.sendMessage("§6Petite voix §7»", "§7§oCe nom est trop long veuillez le raccourcir");
                player.sendMessage("§6Petite voix §7»", "§7§oVotre compte n'est pas configuré veuillez entrer un nom RP !");
                return;
            }
            player.setRPName(message);
            player.setAccountConfigured(1);
            player.getPlayer().setWalkSpeed(0.2F);
            return;
        }
        if (message.startsWith("!")) {
            //TODO: isMuted
            //HRP
            message = message.substring(1);
            if (player.hasPermission("chat.color")) message = ChatColor.translateAlternateColorCodes('&', message);
            BroadcastUtils.broadcastMessage("§b(HRP) §7- " + player.getRank().getPrefix() + player.getName() + " §7» §r" + message);
            return;
        }
        if (!player.getInfo().canChat()) return;
        if (player.getInfo().getRpChat() != null)
            if (player.getInfo().getRpChat().getCurrentMessage().getResponse() != null)
                player.getInfo().getRpChat().getCurrentMessage().getResponse().done(message);
        if (message.startsWith("+")) {
            //Yell
            message = message.substring(1);
            player.say(40, "§l" + message, 20);
            return;
        }
        if (message.startsWith("-")) {
            //Whisper
            message = message.substring(1);
            player.say(5, "§o" + message, 5);
            return;
        }
        player.say(15, message, 10);//Say
    }

    @EventHandler
    public void onSwapHand(PlayerSwapHandItemsEvent event) {
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        event.setCancelled(true);
        player.getClasse().usePouvoir();
    }

    @EventHandler
    public void openChest(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        if (block == null || block.getType() == Material.AIR) return;
        if (block.getType() == Material.TRAPPED_CHEST) {
            if (player.getPlayer().getGameMode() == GameMode.CREATIVE) return;
            Chest chest = (Chest) block.getState();
            if (!chest.getBlockInventory().contains(Material.TRIPWIRE_HOOK)) {
                player.sendMessage("§6Petite voix §7»", "§oTient, ce coffre est déjà ouvert...");
                return;
            }
            event.setCancelled(true);
            int nb = 26;
            if (chest.getBlockInventory().getSize() == (9 * 6)) nb = (9 * 6);
            double level = chest.getBlockInventory().getItem(nb).getAmount();
            if (player.getSkills().getCrochetage() >= level) {
                chest.getBlockInventory().remove(Material.TRIPWIRE_HOOK);
                player.getPlayer().openInventory(chest.getBlockInventory());
                player.getSkills().improveCrochetage(level / 100.0);
            } else {
                Random random = new Random();
                int r = random.nextInt((int) (level + 1));
                System.out.println("Random: " + r + " / level coffre: " + level + " / level player: " + player.getSkills().getCrochetage());
                player.sendMessage("§6Petite voix §7»", "§oEssayons de crocheter ce coffre");
                if (r == level) {
                    chest.getBlockInventory().remove(Material.TRIPWIRE_HOOK);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                        player.getPlayer().openInventory(chest.getBlockInventory());
                        player.sendMessage("§6Petite voix §7»", "§oEt voila, ça n'auras pas été une mince affaire ");
                        player.getSkills().improveCrochetage(level / 100.0);
                    }, 20L);
                } else {
                    player.sendMessage("§6Petite voix §7»", "§oOn dirait que ce coffre est mieux sécurisé que vous ne le pensiez.");
                }
            }
        }
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        EkylaPlayer player = PlayerUtils.getAccount((Player) event.getEntity());
        if (player.getPlayer().getHealth() - event.getFinalDamage() < 1) {
            Location deathLoc = player.getPlayer().getLocation().clone();
            //TODO: teleport player to hospital
            if (event.getDamager() instanceof Player) {
                EkylaPlayer killer = EkylaPlayer.getPlayer((Player) event.getDamager());
                if (killer.getLevel() > player.getLevel())
                    killer.addLevel(player.getLevel() / 100);
                else
                    killer.addLevel((player.getLevel() - killer.getLevel()) / 100);

                if (player.getFactionRole() != FactionRoles.DEFAULT)
                    killer.getFaction().addLevel(player.getLevel() * 2);
                else killer.getFaction().addLevel(player.getLevel());
            }
            if (!(deathLoc.getBlock().getType() == Material.AIR)) deathLoc.add(0, 1, 0);
            Block block = deathLoc.getBlock();
            block.setType(Material.END_ROD);
            ArmorStand armorStand = (ArmorStand) deathLoc.getWorld().spawnEntity(deathLoc.add(0, 1, 1), EntityType.ARMOR_STAND);
            armorStand.setVisible(false);
            armorStand.setCustomName("§c" + player.getUsername());
            armorStand.setCollidable(false);
            armorStand.setCustomNameVisible(true);
            Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                block.setType(Material.AIR);
                armorStand.remove();
            }, 5 * 60 * 20L);
        }
    }

    @EventHandler
    public void onDismount(EntityDismountEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        EkylaPlayer player = PlayerUtils.getAccount((Player) event.getEntity());
        if (player.getMontures().getMonture() != null) player.getMontures().kill();
    }
}
