package fr.ekyla.core.players.npc;

import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;

public enum NPC {

    MEDECIN(EntityType.ZOMBIE, "Médecin", ChatColor.LIGHT_PURPLE, "EntityZombie"),;

    private EntityType type;
    private String name;
    private ChatColor color;
    private String npc;

    NPC(EntityType type, String name, ChatColor color, String npc) {
        this.type = type;
        this.name = name;
        this.color = color;
        this.npc = npc;
    }

    public String getNpc() {
        return npc;
    }

    public EntityType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }
}
