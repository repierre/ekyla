package fr.ekyla.core.players;

public class PlayerTask implements Runnable {

    @Override
    public void run() {
        for (EkylaPlayer player : EkylaPlayer.getEkylaPlayers()) {
            player.updateScoreboard();
            player.addMana(1);
        }
    }

}
