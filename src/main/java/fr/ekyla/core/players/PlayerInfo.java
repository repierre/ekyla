package fr.ekyla.core.players;

import fr.ekyla.core.players.chat.RPChat;

public class PlayerInfo {

    private EkylaPlayer player;
    private boolean canChat = true;
    private RPChat rpChat;

    public PlayerInfo(EkylaPlayer player) {
        this.player = player;
    }

    public boolean canChat() {
        return canChat;
    }

    public void setCanChat(boolean canChat) {
        this.canChat = canChat;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public RPChat getRpChat() {
        return rpChat;
    }

    public void setRpChat(RPChat rpChat) {
        if (rpChat == null) this.rpChat = rpChat;
        while (this.rpChat != null) ;
        this.rpChat = rpChat;
    }
}
