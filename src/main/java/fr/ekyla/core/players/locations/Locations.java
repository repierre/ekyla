package fr.ekyla.core.players.locations;

import fr.ekyla.core.area.Position;

public enum Locations {

    CAPITAL(0, 0, 0, 90, 0),
    HOSPITAL(-281.5, 64.5, 36.5, 0, 0),;

    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    Locations(double x, double y, double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public Position toPosition() {
        return new Position(x, y, z, yaw, pitch);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getYaw() {
        return yaw;
    }

    public float getPitch() {
        return pitch;
    }
}