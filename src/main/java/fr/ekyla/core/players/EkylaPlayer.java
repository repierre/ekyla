package fr.ekyla.core.players;

import fr.ekyla.core.Core;
import fr.ekyla.core.area.Position;
import fr.ekyla.core.chat.ChatUtils;
import fr.ekyla.core.chat.MessageUtils;
import fr.ekyla.core.chat.Reflection;
import fr.ekyla.core.inventories.Inventory;
import fr.ekyla.core.npc.montures.Montures;
import fr.ekyla.core.players.bank.BankAccount;
import fr.ekyla.core.players.classes.Classe;
import fr.ekyla.core.players.classes.MageClasse;
import fr.ekyla.core.players.classes.VagabonClasse;
import fr.ekyla.core.players.factions.Faction;
import fr.ekyla.core.players.factions.FactionRoles;
import fr.ekyla.core.players.npc.NPC;
import fr.ekyla.core.players.skills.Skill;
import fr.ekyla.core.quests.AQuest;
import fr.ekyla.core.scoreboard.ScoreboardSign;
import fr.ekyla.core.spell.Spell;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class EkylaPlayer {

    private static List<EkylaPlayer> ekylaPlayers = new ArrayList<>();
    private UUID uuid;
    private RankList rank;
    private Player player;
    private String RPName;
    private int accountConfigured;
    private double level;
    private double money;
    private int factionId;
    private FactionRoles factionRole;
    private Classe classe;
    private int mana;
    private ScoreboardSign scoreboardSign;
    private BankAccount bankAccount;
    private boolean action;
    private Skill skills;
    private int questStep;
    private AQuest quest;
    private Montures montures;
    private MontureData montureData;
    private PlayerLocation location;
    private PlayerInfo info;
    private List<Spell> spells = new ArrayList<>();

    /**
     * Create an Ekyla player
     *
     * @param player      player
     * @param rank        rank
     * @param RPName      rpname
     * @param factionId   factionId
     * @param factionRole factionRole
     * @param classe      classe
     * @param mana        mana
     * @param level       level
     */
    EkylaPlayer(Player player, RankList rank, int accountConfigured, String RPName, int factionId, FactionRoles factionRole, String classe, int mana, double level, double money, int quest, int questStep) {
        this.rank = rank;
        this.player = player;
        this.uuid = player.getUniqueId();
        this.factionRole = factionRole;
        this.factionId = factionId;
        this.accountConfigured = accountConfigured;
        this.RPName = RPName;
        this.mana = mana;
        this.level = level;
        this.money = money;
        this.action = false;
        this.location = new PlayerLocation(this);
        this.info = new PlayerInfo(this);

        getFaction().addMember(this);

        switch (classe) {
            case "mage":
                this.classe = new MageClasse(this);
                break;
            default:
                this.classe = new VagabonClasse(this);
                break;
        }

        AQuest aQuest = null;
        this.questStep = questStep;

        try {
            Class<?> questClass = Class.forName("fr.ekyla.core.quests.principalQuests.Quest" + quest);
            Constructor<?> questClassConstructor = questClass.getConstructor(EkylaPlayer.class);
            aQuest = (AQuest) questClassConstructor.newInstance(this);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException | ClassNotFoundException ignored) {
            player.kickPlayer("§cUne erreur est survenue lors du chargement de votre profil.\n§cSi ce problème persiste, transmetez ce code d'erreur à un développeur/administrateur (0001)");
        }
        this.quest = aQuest;

        setScoreboardSign();

        if (getPlayer(uuid) != null) getEkylaPlayers().remove(getPlayer(uuid));
        ekylaPlayers.add(this);
    }

    /**
     * Get a player from his username
     *
     * @param username the username of the player
     * @return the player
     */
    public static EkylaPlayer getPlayer(String username) {
        return getPlayer(Bukkit.getPlayer(username));
    }

    /**
     * Get a player from his Bukkit player class
     *
     * @param player the player
     * @return the player
     */
    public static EkylaPlayer getPlayer(Player player) {
        return getPlayer(player.getUniqueId());
    }

    /**
     * Get a player from his UUID
     *
     * @param uuid the uuid of the player
     * @return the player
     */
    public static EkylaPlayer getPlayer(UUID uuid) {
        for (EkylaPlayer ekylaPlayer : ekylaPlayers) if (ekylaPlayer.getUuid().equals(uuid)) return ekylaPlayer;
        return null;
    }

    /**
     * Get all registered players of the server
     *
     * @return list of Ekyla players
     */
    public static List<EkylaPlayer> getEkylaPlayers() {
        return ekylaPlayers;
    }

    /**
     * Open an inventory to the player
     *
     * @param inventory the inventory to open
     */
    public void openInventory(Inventory inventory) {
        org.bukkit.inventory.Inventory inv = Bukkit.createInventory(null, inventory.getSlots(), inventory.getName());
        inventory.getItems().forEach((item) -> inv.setItem(item.getPosition(), item.buildItemStack()));
        player.openInventory(inv);
    }

    public boolean hasSpell(String spellName) {
        for (Spell spell : spells) {
            if (spellName.toLowerCase().contains(spell.getName().toLowerCase())) return true;
        }
        return false;
    }

    public Spell getSpell(String spellName) {
        for (Spell spell : spells) {
            if (spellName.toLowerCase().contains(spell.getName().toLowerCase())) return spell;
        }
        return null;
    }

    /**
     * Add a spell to the current player
     *
     * @param spell the spell
     */
    public void addSpell(Spell spell) {
        spells.add(spell);
    }

    /**
     * get all spells of the current player
     *
     * @return spells
     */
    public List<Spell> getSpells() {
        return new ArrayList<>(spells);
    }

    public PlayerInfo getInfo() {
        return info;
    }

    public EntityLiving summonNpc(NPC npc, Position location) {
        WorldServer s = ((CraftWorld) location.getWorld()).getHandle();
        try {
            Class<?> clazz = Reflection.getNMSClass(npc.getNpc());
            Constructor cons = clazz.getConstructor(World.class);
            EntityLiving ent = (EntityLiving) cons.newInstance(s);
            ent.setCustomName(npc.getName());
            ent.setCustomNameVisible(true);
            ent.setInvisible(false);
            ent.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
            NBTTagCompound tag = new NBTTagCompound();
            if (tag == null) {
                tag = new NBTTagCompound();
            }
            ent.c(tag);
            tag.setInt("NoAI", 1);
            ent.f(tag);
            PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(ent);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
            new PacketPlayOutEntityDestroy(ent.getId());
            return ent;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            player.sendMessage("§cUne erreur est survenue, veuillez transmettre cette erreur à un développeur/administrateur (0002)");
        }
        return null;
    }

    public void removeNpc(int id) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(id));
    }

    /**
     * return the location util
     *
     * @return location
     */
    public PlayerLocation getLocationUtil() {
        return location;
    }

    /**
     * return the quest of the player
     *
     * @return quest
     */
    public AQuest getQuest() {
        return quest;
    }

    /**
     * set the quest of the player
     *
     * @param quest quest
     */
    public void setQuest(AQuest quest) {
        this.quest = quest;
    }

    /**
     * return the skills of the player
     *
     * @return skills
     */
    public Skill getSkills() {
        return skills;
    }

    public void setSkills(Skill skills) {
        this.skills = skills;
    }

    public boolean isAction() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    /**
     * broad cast a rp message from the player in the rayon
     *
     * @param rayon   the rayon
     * @param message the message
     * @param delay   the delay in ticks
     */
    public void say(int rayon, String message, double delay) {
        final String mess = message;
        Collection<Entity> entities = player.getPlayer().getWorld().getNearbyEntities(getPlayer().getLocation(), rayon, rayon, rayon);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> entities.forEach((entity -> {
            MessageUtils messageUtils = new MessageUtils().text(getRank().getColor() + getRPName() + " §r: §7" + mess).tooltip(getRank().getColor() + player.getName()).color(ChatColor.GRAY);
            if (entity instanceof Player)
                messageUtils.send((Player) entity);
        })), (long) delay);
    }

    /**
     * return the bank account
     *
     * @return bankAccount
     */
    public BankAccount getBankAccount() {
        return bankAccount;
    }


    /**
     * set the bank account
     *
     * @param bankAccount bankAccount
     */
    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * has the player an account
     *
     * @return hasAccount
     */
    public boolean hasAccount() {
        return bankAccount != null;
    }

    /**
     * Play a sound to the current player a his position
     *
     * @param sound  the sound
     * @param volume the volume
     * @param pitch  the pitch
     */
    public void playSound(Sound sound, int volume, int pitch) {
        player.playSound(player.getLocation(), sound, volume, pitch);
    }

    /**
     * return the mana of the current player
     *
     * @return mana
     */
    public float getMana() {
        return mana;
    }

    /**
     * Set the mana of the current player
     *
     * @param mana the new mana of the player
     */
    public void setMana(int mana) {
        this.mana = mana;
    }

    /**
     * add mana of the current player
     *
     * @param mana the mana to add
     * @return true if it was added, else, return false
     */
    public boolean addMana(int mana) {
        boolean ret = true;
        if (this.mana >= getMaxMana()) ret = false;
        else this.mana += mana;
        if (getMana() / getMaxMana() >= 1) player.setExp((float) (1));
        else player.setExp((float) (getMana() / getMaxMana()));
        player.setLevel((int) getMana());
        return ret;
    }

    /**
     * return the max mana of the current player
     *
     * @return maxmana
     */
    public double getMaxMana() {
        return level;
    }

    /**
     * remove mana of the current player
     *
     * @param mana the mana to remove
     * @return true if it was removed, else, return false
     */
    public boolean removeMana(int mana) {
        if (this.mana <= 0) return false;
        else this.mana -= mana;
        if (getMana() / getMaxMana() >= 1) player.setExp((float) (1));
        else player.setExp((float) (getMana() / getMaxMana()));
        player.setLevel((int) getMana());
        return true;
    }

    /**
     * return the classe of the current player
     *
     * @return classe
     */
    public Classe getClasse() {
        return classe;
    }

    /**
     * Set the classe of the current player
     *
     * @param classe the desired classe
     */
    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    /**
     * Add a potion effect to the current player
     *
     * @param effectType the type of potion
     * @param duration   the duration
     * @param amplifier  the amplifier
     */
    public void addPotionEffect(PotionEffectType effectType, int duration, int amplifier) {
        player.addPotionEffect(new PotionEffect(effectType, duration, amplifier));
    }

    /**
     * return the faction role of the current player
     *
     * @return factionRole
     */
    public FactionRoles getFactionRole() {
        return factionRole;
    }

    /**
     * set the faction role of the current player
     *
     * @param factionRole the new role
     */
    public void setFactionRole(FactionRoles factionRole) {
        this.factionRole = factionRole;
    }

    /**
     * return the faction of the current player
     *
     * @return faction
     */
    public Faction getFaction() {
        for (Faction factions : Faction.getFactions()) {
            if (factions.getId() == factionId) return factions;
        }
        return Faction.getFactions().get(0);
    }

    /**
     * return the faction id of the current player
     *
     * @return factionId
     */
    public int getFactionId() {
        return factionId;
    }

    /**
     * set the faction id of the current player
     *
     * @param factionId the id of the desired faction
     */
    public void setFactionId(int factionId) {
        this.factionId = factionId;
    }

    /**
     * Get the RP name of the current player
     *
     * @return rpname
     */
    public String getRPName() {
        return RPName;
    }

    public void setRPName(String string) {
        this.RPName = string;
    }

    public int getAccountConfigured() {
        return accountConfigured;
    }

    public void setAccountConfigured(int accountConfigured) {
        this.accountConfigured = accountConfigured;
    }

    /**
     * Set the scoreboard of the current player
     */
    public void setScoreboardSign() {
        this.scoreboardSign = new ScoreboardSign(this, "  §6§lEkyla - MMORpg");
        this.scoreboardSign.create();
        updateScoreboard();
    }

    /**
     * Update the scoreboard of the player
     * <p>
     * Ekyla - MMORpg
     * <p>
     * Nom: rpname
     * Faction: faction
     * Quête: quest
     * Etape de quête: currentStep
     */
    public void updateScoreboard() {
        if (scoreboardSign == null) setScoreboardSign();
        this.scoreboardSign.setLine(0, "");
        this.scoreboardSign.setLine(1, "§7Nom: §b" + getRPName());
        this.scoreboardSign.setLine(2, "§7Faction: " + getFaction().getDisplayName());
        if (quest == null) {
            this.scoreboardSign.setLine(3, "§7Quête: §6Aucune");
            this.scoreboardSign.removeLine(4);
        } else {
            this.scoreboardSign.setLine(3, "§7Quête: §6" + quest.getName());
            this.scoreboardSign.setLine(4, "§7Etape de quête: §6" + quest.getCurrentStep().getName());
        }
    }

    /**
     * Get the scoreboard of the current player
     *
     * @return scoreboardSign
     */
    public ScoreboardSign getScoreboardSign() {
        return scoreboardSign;
    }

    /**
     * Has the player the permission to do something ?
     *
     * @param permission the permission to test
     * @return true or false
     */
    public boolean hasPermission(String permission) {
        return rank.getPermissions().contains(permission);
    }

    /**
     * Get the rank of the current player
     *
     * @return rank
     */
    public RankList getRank() {
        return rank;
    }

    /**
     * Get the UUID of the current player
     *
     * @return uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Get the Bukkit player
     *
     * @return player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Send a notification to the current player
     * It play the ENTITY_EXPERIENCE_ORB_PICKUP sound to the player
     */
    public void sendNotification() {
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }

    /**
     * Send a message to the current player with desired prefix (with ChatColor translate)
     *
     * @param prefix  the prefix of the message
     * @param message the message you want to send
     */
    public void sendMessage(String prefix, String message) {
        sendMessage(prefix + " " + message);
    }

    /**
     * Send a message to the current player with desired prefix (with ChatColor translate)
     *
     * @param prefix  the prefix of the message
     * @param message the message you want to send
     * @param delay   the delay in ticks
     */
    public void sendMessage(String prefix, String message, double delay) {
        sendMessage(prefix + " " + message, delay);
    }

    /**
     * Send a message to the current player with no prefix (with ChatColor translate)
     *
     * @param message the message you want to send
     */
    public void sendMessage(String message) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /**
     * Send a message to the current player with no prefix (with ChatColor translate)
     *
     * @param message the message you want to send
     * @param delay   the delay in ticks
     */
    public void sendMessage(String message, double delay) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> player.sendMessage(ChatColor.translateAlternateColorCodes('&', message)), (long) delay);
    }

    /**
     * Get the name of the current player
     *
     * @return playerName
     */
    public String getUsername() {
        return player.getName();
    }

    /**
     * Get the name of the current player
     * If the player has a nickname, return the nickname else, return the name
     *
     * @return name
     */
    public String getName() {
        return (hasNickname()) ? player.getDisplayName() : player.getName();
    }

    public boolean hasNickname() {
        return (!player.getDisplayName().equalsIgnoreCase(player.getName()) || !player.getDisplayName().equalsIgnoreCase(""));
    }

    /**
     * Send a private message to the current player with the EkylaPlayer object of the sender (with ChatColor translate)
     *
     * @param sender  the sender
     * @param message the message you want to send
     * @param notify  notify the player for the message ? (sendNotification() method)
     */
    public void sendPrivateMessage(EkylaPlayer sender, String message, boolean notify) {
        if (notify) sendNotification();
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "" + sender.getRank().getPrefix() + " §8§l➔ §bVous§8§l: " + message));
    }

    /**
     * Send a private message to the current player with the username of the sender (with ChatColor translate)
     *
     * @param senderName the name of the sender
     * @param message    the message you want to send
     * @param notify     notify the player for the message ? (sendNotification() method)
     */
    @Deprecated
    public void sendPrivateMessage(String senderName, String message, boolean notify) {
        if (notify) sendNotification();
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "" + senderName + " §8§l➔ §bVous§8§l: " + message));
    }

    /**
     * Get the level of the current player
     *
     * @return level
     */
    public double getLevel() {
        return level;
    }

    /**
     * Set the level of the current player
     *
     * @param level the level of the player
     */
    public void setLevel(double level) {
        this.level = level;
    }

    /**
     * add experience to the player
     *
     * @param px the experience
     */
    public void addExperience(int px) {
        this.level += px / 100F;
    }

    /**
     * Get the money of the current player
     *
     * @return money
     */
    public double getMoney() {
        return money;
    }

    /**
     * Set the money of the current player
     *
     * @param money the new money of the player
     */
    public void setMoney(double money) {
        this.money = money;
    }

    /**
     * Add money to the current player
     *
     * @param money the money to add
     */
    public void addMoney(double money) {
        this.money += money;
    }

    /**
     * Remov emoney from the current player
     *
     * @param money the money to remove
     */
    public void removeMoney(double money) {
        this.money -= money;
    }

    /**
     * Push the player to the database
     */
    public void pushToDatabase() {
        PlayerUtils.updatePlayer(this);
    }

    /**
     * Disconnect the player
     * If he is connected he is kicked
     */
    public void disconnect() {
        if (getMontures().getMonture() != null && getMontures().getMonture().getPassengers().contains(player)) {
            getMontures().getMonture().removePassenger(player.getPlayer());
        }
        pushToDatabase();
        if (player.isOnline()) player.kickPlayer("§cVous avez été deconnecté");
        getEkylaPlayers().remove(this);
    }

    /**
     * Kick the player from the server
     *
     * @param lines displayed lines on the screen of the player after kicking
     */
    public void kick(String... lines) {
        StringBuilder builder = new StringBuilder();
        for (String line : lines) {
            builder.append(line).append("\n");
        }
        player.kickPlayer(builder.toString());
        disconnect();
    }

    @Override
    public String toString() {
        return ChatColor.translateAlternateColorCodes('&', "§7" + ChatUtils.getCenteredText("Votre profil") + "\n" +
                "§b▶ §6Pseudo: " + getRank().getColor() + getUsername() + "\n" +
                "§b▶ §6Surnom: " + (hasNickname() ? getRank().getColor() + player.getName() : "§7Aucun") + "\n" +
                "§b▶ §6Nom RP: §b" + getRPName() + "\n" +
                "§b▶ §6Role: §b" + "Aucun" + "\n" +
                "§b▶ §6Faction: " + getFaction().getDisplayName() + "\n" +
                "§b▶ §6Role dans la " + getFaction().getDisplayName() + "§6: §b" + getFactionRole().getName() + "\n");
    }

    /**
     * return the amplifier
     *
     * @return amplifier
     */
    public int getAmplifier() {
        int amplifier = 1;
        if (level >= 1 && level < 20) amplifier = 1;
        if (level >= 20 && level < 50) amplifier = 2;
        if (level >= 50 && level < 100) amplifier = 3;
        if (level >= 100) amplifier = 4;
        return amplifier;
    }

    /**
     * has the player the required money
     *
     * @param price the price
     * @return has money ?
     */
    public boolean hasMoney(double price) {
        return (getMoney() - price) >= 0;
    }

    /**
     * return the quest step
     *
     * @return questStep
     */
    public int getQuestStep() {
        return questStep;
    }

    /**
     * set the quest step
     *
     * @param questStep questStep
     */
    public void setQuestStep(int questStep) {
        this.questStep = questStep;
    }

    public Montures getMontures() {
        return montures;
    }

    public void setMontures(Montures montures) {
        this.montures = montures;
    }

    public MontureData getMontureData() {
        return montureData;
    }

    public void setMontureData(MontureData montureData) {
        this.montureData = montureData;
    }

    public void addLevel(double level) {
        this.level += level;
    }

    public boolean hasMana(int manaCost) {
        return (getMana() - manaCost) >= 0;
    }
}
