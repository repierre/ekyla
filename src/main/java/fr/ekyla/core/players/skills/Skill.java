package fr.ekyla.core.players.skills;

import fr.ekyla.core.players.EkylaPlayer;

public class Skill {

    private EkylaPlayer player;

    private double crochetage;
    private double spell;
    private double brute;
    private double vol;
    private boolean canVol;

    public Skill(EkylaPlayer player, double crochetage, double spell, double brute, double vol) {
        this.player = player;
        this.crochetage = crochetage;
        this.spell = spell;
        this.brute = brute;
        this.vol = vol;
        this.canVol = true;
    }

    public double getVol() {
        return vol;
    }

    public void setVol(double vol) {
        this.vol = vol;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public void setPlayer(EkylaPlayer player) {
        this.player = player;
    }

    public double getCrochetage() {
        return crochetage;
    }

    public void setCrochetage(double crochetage) {
        this.crochetage = crochetage;
    }

    public double getSpell() {
        return spell;
    }

    public void setSpell(double spell) {
        this.spell = spell;
    }

    public double getBrute() {
        return brute;
    }

    public void setBrute(double brute) {
        this.brute = brute;
    }

    public void improveCrochetage(double crochetage) {
        this.crochetage += crochetage;
    }

    public void improveSpell(double spell) {
        this.spell += spell;
    }

    public void improveBrute(double brute) {
        this.brute += brute;
    }

    public void improveVol(double vol) {
        this.vol += vol;
    }

    public boolean canVol() {
        return canVol;
    }

    public void setCanVol(boolean canVol) {
        this.canVol = canVol;
    }
}
