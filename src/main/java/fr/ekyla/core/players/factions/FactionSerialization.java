package fr.ekyla.core.players.factions;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * File <b>FactionSerialization</b> located on fr.ekyla.core.players.factions
 * FactionSerialization is a part of ekyla.
 * <p>
 * Copyright (c) 2017 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 05/03/2018 at 17:11
 */
public class FactionSerialization extends TypeAdapter<Faction> {

    @Override
    public void write(JsonWriter jsonWriter, Faction faction) throws IOException {
        jsonWriter.beginObject();
        jsonWriter.name("id").value(faction.getId());
        jsonWriter.name("name").value(faction.getName());
        jsonWriter.name("color").value("" + faction.getColor().getChar());
        jsonWriter.name("imposition").value(faction.getImposition());
        jsonWriter.name("money").value(faction.getMoney());
        jsonWriter.name("level").value(faction.getLevel());
        jsonWriter.endObject();
    }

    @Override
    public Faction read(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();
        int id = 0;
        String name = "";
        String color = "";
        float imposition = 0.0f;
        int money = 0;
        double level = 1.0;

        while (jsonReader.hasNext()) {
            switch (jsonReader.nextName()) {
                case "id":
                    id = jsonReader.nextInt();
                    break;
                case "name":
                    name = jsonReader.nextString();
                    break;
                case "color":
                    color = jsonReader.nextString();
                    break;
                case "imposition":
                    imposition = (float) jsonReader.nextDouble();
                    break;
                case "money":
                    money = jsonReader.nextInt();
                    break;
                case "level":
                    level = jsonReader.nextDouble();
                    break;
            }
        }
        jsonReader.endObject();

        return new Faction(id, name, color, imposition, money, level);
    }

}
