package fr.ekyla.core.players.factions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * File <b>GsonUtil</b> located on fr.ekyla.core.utils
 * GsonUtil is a part of ekyla.
 * <p>
 * Copyright (c) 2017 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 05/03/2018 at 17:42
 */
@SuppressWarnings("UnusedReturnValue")
public class FactionGson {

    private Gson gson;

    public FactionGson() {
        this.gson = new GsonBuilder()
                .disableHtmlEscaping()
                .serializeNulls()
                .setPrettyPrinting()
                .registerTypeAdapter(Faction.class, new FactionSerialization())
                .create();
    }

    public String toJson(Faction faction) {
        return this.gson.toJson(faction);
    }

    public Faction fromJson(String json) {
        return this.gson.fromJson(json, Faction.class);
    }

}
