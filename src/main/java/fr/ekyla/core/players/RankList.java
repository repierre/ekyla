package fr.ekyla.core.players;

import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum RankList {

    PLAYER(0, "Joueur", "[Joueur]", ChatColor.GRAY, Arrays.asList("monture.spawn", "")),
    HELPEUR(10, "Helpeur", "[Helpeur]", ChatColor.YELLOW, Arrays.asList("server.connection.message", ""), PLAYER),
    MODERATOR(50, "Modérateur", "[Modérateur]", ChatColor.DARK_BLUE, Arrays.asList("chat.color", ""), HELPEUR),
    ADMIN(100, "Admin", "[Admin]", ChatColor.RED, Arrays.asList("npc.spawn", "rprename.player", "roach.monture", ""), MODERATOR);

    private int power;
    private String name;
    private String prefix;
    private ChatColor color;
    private List<String> permissions;
    private RankList[] herit;

    RankList(int power, String name, String prefix, ChatColor color, List<String> permissions, RankList... herit) {
        this.power = power;
        this.name = name;
        this.prefix = prefix;
        this.permissions = permissions;
        this.herit = herit;
        this.color = color;
    }

    public static RankList getFromPower(int power) {
        for (RankList rankList : values()) if (rankList.getPower() == power) return rankList;
        return RankList.PLAYER;
    }

    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return getColor() + prefix + " ";
    }

    public List<String> getPermissions() {
        List<String> perm = new ArrayList<>();
        for (RankList rankList : getHerit()) {
            perm.addAll(rankList.getPermissions());
        }
        perm.addAll(permissions);
        return perm;
    }

    public RankList[] getHerit() {
        return herit;
    }

    public ChatColor getColor() {
        return color;
    }

}
