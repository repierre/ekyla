package fr.ekyla.core.players;

import fr.ekyla.core.npc.AbstractNpc;
import fr.ekyla.core.players.locations.Locations;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

public class PlayerLocation {

    private EkylaPlayer player;

    PlayerLocation(EkylaPlayer player) {
        this.player = player;
    }

    public void teleport(double x, double y, double z) {
        player.getPlayer().getLocation().setX(x);
        player.getPlayer().getLocation().setY(y);
        player.getPlayer().getLocation().setZ(z);
    }

    public void teleport(Locations location) {
        player.getPlayer().teleport(location.toPosition().toLocation());
    }

    public void teleport(EkylaPlayer player) {
        this.player.getPlayer().teleport(player.getPlayer());
    }

    public void teleport(AbstractNpc npc) {
        player.getPlayer().getLocation().setX(npc.getLocation().getX());
        player.getPlayer().getLocation().setY(npc.getLocation().getY());
        player.getPlayer().getLocation().setZ(npc.getLocation().getZ());
        player.getPlayer().getLocation().setDirection(npc.getLocation().getDirection());
    }

    public void teleport(Entity entity) {
        player.getPlayer().teleport(entity);
    }

    public Location getLocation() {
        return player.getPlayer().getLocation();
    }

}
