package fr.ekyla.core.players.classes;

import fr.ekyla.core.players.EkylaPlayer;

public abstract class Classe {

    //private static List<Spell> reservedSpells;
    private EkylaPlayer player;
    private long lastUse = 0;
    private int timer;
    private String name;

    Classe(EkylaPlayer player, int timer, String name) {
        this.player = player;
        this.timer = timer;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    abstract void pouvoir();

    public void usePouvoir() {
        if (canUsePouvoir()) {
            pouvoir();
            lastUse = System.currentTimeMillis();
            player.sendMessage("§6Petite voix §7» §7§oVous avez lancé votre pouvoir. Vous pourrez le réutiliser dans " + getRemainingTime() + " secondes.");
        } else {
            player.sendMessage("§6Petite voix §7» §7§oVous devez attendre un petit peu avant de réutiliser votre pouvoir. Réessayez dans " + getRemainingTime() + " secondes.");
        }
    }

    public boolean canUsePouvoir() {
        return lastUse == 0 || getRemainingTime() < 0;
    }

    public long getRemainingTime() {
        return ((lastUse / 1000 + timer) - System.currentTimeMillis() / 1000);
    }

}
