package fr.ekyla.core.players.classes;

import fr.ekyla.core.Core;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

public class MageClasse extends Classe {

    private int time = getPlayer().getAmplifier() * 2 * 2;
    private int task = 0;

    public MageClasse(EkylaPlayer player) {
        super(player, 120, "mage");
    }

    @Override
    public void pouvoir() {
        getPlayer().playSound(Sound.BLOCK_WATER_AMBIENT, 1, 1);
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.getInstance(), () -> {
            if (time == 0) Bukkit.getScheduler().cancelTask(task);
            getPlayer().addMana(1);
            time--;
        }, 0L, 10L);
    }

}
