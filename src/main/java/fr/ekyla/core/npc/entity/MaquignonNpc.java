package fr.ekyla.core.npc.entity;

import fr.ekyla.core.npc.AbstractNpc;
import fr.ekyla.core.npc.saver.NPCSave;
import org.bukkit.Location;
import org.bukkit.entity.Villager;

public class MaquignonNpc extends AbstractNpc {

    public MaquignonNpc(String name, Location location) {
        super("maquignon", name, location);
    }

    @Override
    public void spawn() {
        npc = location.getWorld().spawn(location, Villager.class);
        Villager villager = (Villager) npc;
        villager.setProfession(Villager.Profession.NITWIT);
        villager.getLocation().setYaw(location.getYaw());
        villager.getLocation().setPitch(location.getPitch());
        villager.setAdult();
        villager.setAI(false);
        villager.setCollidable(false);
        villager.setAgeLock(true);
        villager.setCustomName(getName());
        villager.setCustomNameVisible(true);
        villager.setInvulnerable(true);
        villager.teleport(location);
    }
}