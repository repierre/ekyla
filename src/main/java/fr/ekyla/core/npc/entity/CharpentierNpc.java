package fr.ekyla.core.npc.entity;

import fr.ekyla.core.npc.MerchantNpc;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.block.PistonMoveReaction;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.*;

public class CharpentierNpc extends MerchantNpc {

    public CharpentierNpc(String name, Location location) {
        super("charpentier", name, location, new Random().nextInt(150));
    }

    @Override
    public void spawn() {
        npc = location.getWorld().spawn(location, Villager.class);
        Villager villager = (Villager) npc;
        villager.setProfession(Villager.Profession.NITWIT);
        villager.getLocation().setYaw(location.getYaw());
        villager.getLocation().setPitch(location.getPitch());
        villager.setAdult();
        villager.setAI(false);
        villager.setCollidable(false);
        villager.setAgeLock(true);
        villager.setCustomName(getName());
        villager.setCustomNameVisible(true);
        villager.setInvulnerable(true);
        villager.teleport(location);
    }

}
