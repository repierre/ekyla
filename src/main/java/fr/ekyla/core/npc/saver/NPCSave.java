package fr.ekyla.core.npc.saver;

public enum NPCSave {

    GOLD("gold"),;

    private String name;

    NPCSave(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
