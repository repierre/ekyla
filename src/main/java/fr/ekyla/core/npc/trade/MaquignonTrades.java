package fr.ekyla.core.npc.trade;

import fr.ekyla.core.items.Itembuilder;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class MaquignonTrades {

    public static void setMaquignonItems(EkylaPlayer player) {
        Inventory inventory = player.getPlayer().getOpenInventory().getTopInventory();
        inventory.setItem(53, new Itembuilder(Material.BOOK).setName("§6Mode Vente indisponible").setLore(Arrays.asList("§6Contenu de votre bourse: §e" + player.getMoney() + " PO")).buildItemStack());

        if (player.getMontureData().getColorWHITE() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("WHITE"))
                inventory.setItem((0 * 9) + 0, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Blanc").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 0, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Blanc").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 0, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Blanc").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getColorCREAMY() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("CREAMY"))
                inventory.setItem((0 * 9) + 1, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Crémeux").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 1, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Crémeux").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 1, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Crémeux").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getColorCHESTNUT() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("CHESTNUT"))
                inventory.setItem((0 * 9) + 2, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Noisette").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 2, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Noisette").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 2, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Noisette").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getColorBROWN() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("BROWN"))
                inventory.setItem((0 * 9) + 3, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Marron Clair").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 3, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Marron Clair").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 3, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Marron Clair").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getColorBLACK() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("BLACK"))
                inventory.setItem((0 * 9) + 4, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Noir").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 4, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Noir").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 4, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Noir").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getColorGRAY() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("GRAY"))
                inventory.setItem((0 * 9) + 5, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Gris").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 5, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Gris").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 5, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Gris").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getColorDARK_BROWN() == 1) {
            if (player.getMontures().getColor().equalsIgnoreCase("DARK_BROWN"))
                inventory.setItem((0 * 9) + 6, new Itembuilder(Material.WOOL).setData(13).setName("§6Couleur : Marron Foncé").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((0 * 9) + 6, new Itembuilder(Material.WOOL).setData(7).setName("§6Couleur : Marron Foncé").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((0 * 9) + 6, new Itembuilder(Material.WOOL).setData(14).setName("§6Couleur : Marron Foncé").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());

        if (player.getMontureData().getStyleNONE() == 1) {
            if (player.getMontures().getStyle().equalsIgnoreCase("NONE"))
                inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOL).setData(13).setName("§6Style : Aucun").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOL).setData(7).setName("§6Style : Aucun").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOL).setData(14).setName("§6Style : Aucun").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getStyleWHITE() == 1) {
            if (player.getMontures().getStyle().equalsIgnoreCase("WHITE"))
                inventory.setItem((1 * 9) + 1, new Itembuilder(Material.WOOL).setData(13).setName("§6Style : Blanc").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((1 * 9) + 1, new Itembuilder(Material.WOOL).setData(7).setName("§6Style : Blanc").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((1 * 9) + 1, new Itembuilder(Material.WOOL).setData(14).setName("§6Style : Blanc").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getStyleWHITEFIELD() == 1) {
            if (player.getMontures().getStyle().equalsIgnoreCase("WHITEFIELD"))
                inventory.setItem((1 * 9) + 2, new Itembuilder(Material.WOOL).setData(13).setName("§6Style : Taches Blanches").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((1 * 9) + 2, new Itembuilder(Material.WOOL).setData(7).setName("§6Style : Taches Blanches").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((1 * 9) + 2, new Itembuilder(Material.WOOL).setData(14).setName("§6Style : Taches Blanches").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getStyleWHITE_DOTS() == 1) {
            if (player.getMontures().getStyle().equalsIgnoreCase("WHITE_DOTS"))
                inventory.setItem((1 * 9) + 3, new Itembuilder(Material.WOOL).setData(13).setName("§6Style : Points Blancs").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((1 * 9) + 3, new Itembuilder(Material.WOOL).setData(7).setName("§6Style : Points Blancs").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((1 * 9) + 3, new Itembuilder(Material.WOOL).setData(14).setName("§6Style : Points Blancs").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getStyleBLACK_DOTS() == 1) {
            if (player.getMontures().getStyle().equalsIgnoreCase("BLACK_DOTS"))
                inventory.setItem((1 * 9) + 4, new Itembuilder(Material.WOOL).setData(13).setName("§6Style : Points Noirs").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((1 * 9) + 4, new Itembuilder(Material.WOOL).setData(7).setName("§6Style : Points Noirs").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((1 * 9) + 4, new Itembuilder(Material.WOOL).setData(14).setName("§6Style : Points Noirs").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());

        if (player.getMontureData().getArmorIRON_BARDING() == 1) {
            if (player.getMontures().getArmor().equals("IRON_BARDING"))
                inventory.setItem((2 * 9) + 0, new Itembuilder(Material.WOOL).setData(13).setName("§6Armure : Fer").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((2 * 9) + 0, new Itembuilder(Material.WOOL).setData(7).setName("§6Armure : Fer").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((2 * 9) + 0, new Itembuilder(Material.WOOL).setData(14).setName("§6Armure : Fer").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getArmorGOLD_BARDING() == 1) {
            if (player.getMontures().getArmor().equals("GOLD_BARDING"))
                inventory.setItem((2 * 9) + 1, new Itembuilder(Material.WOOL).setData(13).setName("§6Armure : Or").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((2 * 9) + 1, new Itembuilder(Material.WOOL).setData(7).setName("§6Armure : Or").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((2 * 9) + 1, new Itembuilder(Material.WOOL).setData(14).setName("§6Armure : Or").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        if (player.getMontureData().getArmorDIAMOND_BARDING() == 1) {
            if (player.getMontures().getArmor().equals("DIAMOND_BARDING"))
                inventory.setItem((2 * 9) + 2, new Itembuilder(Material.WOOL).setData(13).setName("§6Armure : Diamant").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((2 * 9) + 2, new Itembuilder(Material.WOOL).setData(7).setName("§6Armure : Diamant").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        } else
            inventory.setItem((2 * 9) + 2, new Itembuilder(Material.WOOL).setData(14).setName("§6Armure : Diamant").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());

        if (player.getMontures().getSpeed() <= 0.25F)
            inventory.setItem((3 * 9) + 0, new Itembuilder(Material.WOOL).setData(14).setName("§6Vitesse : 120%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        else {
            if (player.getMontures().getSpeed() <= 0.3F)
                inventory.setItem((3 * 9) + 0, new Itembuilder(Material.WOOL).setData(14).setName("§6Vitesse : 140%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
            else {
                if (player.getMontures().getSpeed() <= 0.35F)
                    inventory.setItem((3 * 9) + 0, new Itembuilder(Material.WOOL).setData(14).setName("§6Vitesse : 160%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
                else
                    inventory.setItem((3 * 9) + 0, new Itembuilder(Material.WOOL).setData(13).setName("§6Vitesse : Niveau max").setLore(Arrays.asList(" ")).buildItemStack());
            }
        }

        if (player.getMontures().getJump() <= 0.5F)
            inventory.setItem((3 * 9) + 1, new Itembuilder(Material.WOOL).setData(14).setName("§6Saut : 120%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        else {
            if (player.getMontures().getJump() <= 0.6F)
                inventory.setItem((3 * 9) + 1, new Itembuilder(Material.WOOL).setData(14).setName("§6Saut : 140%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
            else {
                if (player.getMontures().getJump() <= 0.7F)
                    inventory.setItem((3 * 9) + 1, new Itembuilder(Material.WOOL).setData(14).setName("§6Saut : 160%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
                else
                    inventory.setItem((3 * 9) + 1, new Itembuilder(Material.WOOL).setData(13).setName("§6Saut : Niveau max").setLore(Arrays.asList(" ")).buildItemStack());
            }
        }

        if (player.getMontures().getHealth() <= 20F)
            inventory.setItem((3 * 9) + 2, new Itembuilder(Material.WOOL).setData(14).setName("§6Vie : 120%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
        else {
            if (player.getMontures().getHealth() <= 24F)
                inventory.setItem((3 * 9) + 2, new Itembuilder(Material.WOOL).setData(14).setName("§6Vie : 140%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
            else {
                if (player.getMontures().getHealth() <= 28F)
                    inventory.setItem((3 * 9) + 2, new Itembuilder(Material.WOOL).setData(14).setName("§6Vie : 160%").setLore(Arrays.asList("§4Acheter: §e15 PO")).buildItemStack());
                else
                    inventory.setItem((3 * 9) + 2, new Itembuilder(Material.WOOL).setData(13).setName("§6Vie : Niveau max").setLore(Arrays.asList(" ")).buildItemStack());
            }
        }

        inventory.setItem((5 * 9) + 0, new Itembuilder(Material.WOOL).setData(0).setName("§6Monture : Cheval").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        inventory.setItem((5 * 9) + 1, new Itembuilder(Material.WOOL).setData(0).setName("§6Monture : Âne").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        inventory.setItem((5 * 9) + 2, new Itembuilder(Material.WOOL).setData(0).setName("§6Monture : Mule").setLore(Arrays.asList("§2Appliquer")).buildItemStack());

        if (player.hasPermission("roach.monture")) {
            if (player.getMontures().getType().equalsIgnoreCase("Roach"))
                inventory.setItem((5 * 9) + 7, new Itembuilder(Material.WOOL).setData(13).setName("§6Monture : Roach").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
            else
                inventory.setItem((5 * 9) + 7, new Itembuilder(Material.WOOL).setData(7).setName("§6Monture : Roach").setLore(Arrays.asList("§2Appliquer")).buildItemStack());
        }
    }
}
