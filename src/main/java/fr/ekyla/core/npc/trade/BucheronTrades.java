package fr.ekyla.core.npc.trade;

import fr.ekyla.core.items.Itembuilder;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class BucheronTrades {

    public static void setBucheronAchatItems(EkylaPlayer player) {
        Inventory inventory = player.getPlayer().getOpenInventory().getTopInventory();
        inventory.setItem(53, new Itembuilder(Material.BOOK).setName("§6Passer en mode Vente").setLore(Arrays.asList("§6Contenu de votre bourse: §e" + player.getMoney() + " PO")).buildItemStack());
        inventory.setItem((0 * 9) + 0, new Itembuilder(Material.LOG).setData(0).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 1, new Itembuilder(Material.LOG).setData(1).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 2, new Itembuilder(Material.LOG).setData(2).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 3, new Itembuilder(Material.LOG).setData(3).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 4, new Itembuilder(Material.LOG_2).setData(0).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 5, new Itembuilder(Material.LOG_2).setData(1).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOD).setData(0).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 1, new Itembuilder(Material.WOOD).setData(1).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 2, new Itembuilder(Material.WOOD).setData(2).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 3, new Itembuilder(Material.WOOD).setData(3).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 4, new Itembuilder(Material.WOOD).setData(4).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 5, new Itembuilder(Material.WOOD).setData(5).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 0, new Itembuilder(Material.WOOD_STAIRS).setLore(Arrays.asList("§6Acheter: §e22.5 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 1, new Itembuilder(Material.SPRUCE_WOOD_STAIRS).setLore(Arrays.asList("§6Acheter: §e22.5 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 2, new Itembuilder(Material.BIRCH_WOOD_STAIRS).setLore(Arrays.asList("§6Acheter: §e22.5 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 3, new Itembuilder(Material.JUNGLE_WOOD_STAIRS).setLore(Arrays.asList("§6Acheter: §e22.5 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 4, new Itembuilder(Material.ACACIA_STAIRS).setLore(Arrays.asList("§6Acheter: §e22.5 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 5, new Itembuilder(Material.DARK_OAK_STAIRS).setLore(Arrays.asList("§6Acheter: §e22.5 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 0, new Itembuilder(Material.WOOD_STEP).setData(0).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 1, new Itembuilder(Material.WOOD_STEP).setData(1).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 2, new Itembuilder(Material.WOOD_STEP).setData(2).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 3, new Itembuilder(Material.WOOD_STEP).setData(3).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 4, new Itembuilder(Material.WOOD_STEP).setData(4).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 5, new Itembuilder(Material.WOOD_STEP).setData(5).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
    }

    public static void setBucheronVenteItems(EkylaPlayer player) {
        Inventory inventory = player.getPlayer().getOpenInventory().getTopInventory();
        inventory.setItem(53, new Itembuilder(Material.BOOK).setName("§6Passer en mode Achat").setLore(Arrays.asList("§6Contenu de votre bourse: §e" + player.getMoney() + " PO")).buildItemStack());
        inventory.setItem((0 * 9) + 0, new Itembuilder(Material.LOG).setData(0).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 1, new Itembuilder(Material.LOG).setData(1).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 2, new Itembuilder(Material.LOG).setData(2).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 3, new Itembuilder(Material.LOG).setData(3).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 4, new Itembuilder(Material.LOG_2).setData(0).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 5, new Itembuilder(Material.LOG_2).setData(1).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOD).setData(0).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 1, new Itembuilder(Material.WOOD).setData(1).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 2, new Itembuilder(Material.WOOD).setData(2).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 3, new Itembuilder(Material.WOOD).setData(3).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 4, new Itembuilder(Material.WOOD).setData(4).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 5, new Itembuilder(Material.WOOD).setData(5).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 0, new Itembuilder(Material.WOOD_STAIRS).setLore(Arrays.asList("§6Vendre: §e18 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 1, new Itembuilder(Material.SPRUCE_WOOD_STAIRS).setLore(Arrays.asList("§6Vendre: §e18 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 2, new Itembuilder(Material.BIRCH_WOOD_STAIRS).setLore(Arrays.asList("§6Vendre: §e18 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 3, new Itembuilder(Material.JUNGLE_WOOD_STAIRS).setLore(Arrays.asList("§6Vendre: §e18 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 4, new Itembuilder(Material.ACACIA_STAIRS).setLore(Arrays.asList("§6Vendre: §e18 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 5, new Itembuilder(Material.DARK_OAK_STAIRS).setLore(Arrays.asList("§6Vendre: §e18 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 0, new Itembuilder(Material.WOOD_STEP).setData(0).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 1, new Itembuilder(Material.WOOD_STEP).setData(1).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 2, new Itembuilder(Material.WOOD_STEP).setData(2).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 3, new Itembuilder(Material.WOOD_STEP).setData(3).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 4, new Itembuilder(Material.WOOD_STEP).setData(4).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 5, new Itembuilder(Material.WOOD_STEP).setData(5).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
    }
}
