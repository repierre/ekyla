package fr.ekyla.core.npc.messages;

import java.util.Random;

public class BanquierMessages {

    public static final String[] depossucces = new String[]{
            "Hum... Voila, votre dépot a été effectué.",
            "Ces §e%amount PO§7 viennent d'être ajoutés à votre compte."
    };
    public static final String[] deposfailrest = new String[]{
            "Ho... Regardez votre bourse ! Elle ne contient pas les §e%amount PO§7 que vous me demandez de déposer.",
            "Vous n'avez pas assez de §ePO§7 dans votre bourse pour cette transaction."
    };
    public static final String[] deposrest = new String[]{
            "Elle ne contient que §e%money PO§7... Je vous propose de les déposer dans ce cas.",
            "Je dépose donc vos §e%money PO§7 restants."
    };
    public static final String[] deposfail = new String[]{
            "Votre bourse est vide !",
            "Vous n'avez plus rien dans votre bourse."
    };
    public static final String[] retraitsucces = new String[]{
            "Voilà vos §e%amount PO§7.",
            "Et §e%amount PO§7 pour vous !"
    };
    public static final String[] retraitfailrest = new String[]{
            "Hum... d'après mes fiches, votre compte ne contient pas assez d'argent.",
            "Vous n'avez pas assez de §ePO§7 sur votre compte pour cette transaction."
    };
    public static final String[] retraitrest = new String[]{
            "Il ne contient que §e%bank PO§7... Je vous propose de les retirer dans ce cas.",
            "Voici donc vos §e%bank PO§7 restants."
    };
    public static final String[] retraitfail = new String[]{
            "Hum... il semble que votre compte est vide.",
            "Vous n'avez plus rien sur votre compte."
    };
    public static final String[] openinventoryday = new String[]{
            "Bonjour %pname, est-ce pour un dépôt ou un retrait?",
            "Bonjour %pname, que puis-je faire pour vous?"
    };
    public static final String[] openinventorynight = new String[]{
            "Bonsoir %pname, est-ce pour un dépôt ou un retrait?",
            "Bonsoir %pname, que puis-je faire pour vous?"
    };
    public static final String[] closeinventorymoney = new String[]{
            "Bonne journée Mr...Mr.%pname.",
            "Faites bon usage de cet argent Mr.%pname."
    };
    public static final String[] closeinventorynomoney = new String[]{
            "Bonne journée Mr...Mr.%pname.",
            "Au revoir Mr.%pname !"
    };

    public static String getBanquierRandomMessage(String[] array) {
        if (array.length == 0) return "";
        Random random = new Random();
        int nb = random.nextInt(array.length);
        String message = array[nb];
        if (message == null || message.equals("")) {
            System.out.println("Erreur : getBanquierRandomMessage");
            return "";
        }
        return message;
    }
}
