package fr.ekyla.core.npc.messages;

import java.util.Random;

public class BucheronMessages {

    public static final String[] buyfail = new String[]{
            "Hola mon gaillard ! Vous n'avez pas assez d'argent pour acheter ceci !",
            "Mais dites moi... vous croyez acheter cela avec seulement §e%money PO§7."
    };
    public static final String[] buysucess = new String[]{
            "Et voila, le tout pour la modique somme de §e%price PO§7.",
            "Voici ce que vous avez demandé."
    };
    public static final String[] sellfail = new String[]{
            "Hola mon gaillard ! Tentez-vous de me voler ? Vous n'avez pas ce que je demande.",
            "Si vous ne me présentez pas la marchandise attendue, je ne peux vous payer."
    };
    public static final String[] sellsucess = new String[]{
            "Et §e%price PO §7pour ceci !",
            "Voila vos §e%price PO §7comme demandé."
    };
    public static final String[] openinventoryday = new String[]{
            "Bonjour %pname !",
            "Quel plaisir de vous revoir %pname."
    };
    public static final String[] openinventorynight = new String[]{
            "Bonsoir %pname !",
            "Quel plaisir de vous revoir %pname."
    };
    public static final String[] closeinventoryrainy = new String[]{
            "A une prochaine %pname",
            "Bonne chance !"
    };
    public static final String[] closeinventorysunny = new String[]{
            "A une prochaine %pname.",
            "Au revoir et profitez bien de cette belle journée %pname."
    };

    public static String getBucheronRandomMessage(String[] array) {
        if (array.length == 0) return "";
        Random random = new Random();
        int nb = random.nextInt(array.length);
        String message = array[nb];
        if (message == null || message.equals("")) {
            System.out.println("Erreur : getBucheronRandomMessage");
            return "";
        }
        return message;
    }
}
