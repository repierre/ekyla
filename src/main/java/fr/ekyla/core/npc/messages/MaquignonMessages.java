package fr.ekyla.core.npc.messages;

import java.util.Random;

public class MaquignonMessages {

    public static final String[] buyfail = new String[]{
            "Hola %pname ! Il vous manque de l'argent pour ceci !",
            "Comment ? vous pensez pouvoir acheter cela avec seulement §e%money PO§7."
    };
    public static final String[] buysucess = new String[]{
            "L'élevage des chevaux est tout un art. Vous vous en tirez à bon compte pour seulement §e%price PO§7.",
            "Et voici pour votre monture !"
    };
    public static final String[] openinventoryday = new String[]{
            "Bonjour %pname ! que puis-je faire pour vous ?",
            "%pname ! Quel plaisir me vaut cette visite ?"
    };
    public static final String[] openinventorynight = new String[]{
            "Bonsoir %pname ! que puis-je faire pour vous ?",
            "%pname ! Quel plaisir me vaut cette visite ?"
    };
    public static final String[] closeinventoryrainy = new String[]{
            "A bientôt %pname",
            "Bonne continuation %pname !"
    };
    public static final String[] closeinventorysunny = new String[]{
            "A bientôt %pname.",
            "Au revoir et profitez en tant qu'il fait beau !"
    };

    public static String getMaquignonRandomMessage(String[] array) {
        if (array.length == 0) return "";
        Random random = new Random();
        int nb = random.nextInt(array.length);
        String message = array[nb];
        if (message == null || message.equals("")) {
            System.out.println("Erreur : getMaquignonRandomMessage");
            return "";
        }
        return message;
    }
}
