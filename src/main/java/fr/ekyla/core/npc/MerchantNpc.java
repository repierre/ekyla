package fr.ekyla.core.npc;

import fr.ekyla.core.npc.saver.NPCSave;
import fr.ekyla.core.utils.JsonWriter;
import org.bukkit.Location;

public abstract class MerchantNpc extends AbstractNpc {

    private int gold;

    public MerchantNpc(String type, String name, Location location, int gold) {
        super(type, name, location);
        this.gold = gold;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
