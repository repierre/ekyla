package fr.ekyla.core.npc.montures;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Donkey;
import org.bukkit.inventory.ItemStack;

public class Donkeys extends Montures{

    public Donkeys(EkylaPlayer player, String type, String name, String color, String style, String armor, float speed, float jump, float health) {
        super(player, type, name, color, style, armor, speed, jump, health);
    }

    public static void spawnDonkey(EkylaPlayer player) {
        Donkey horse = player.getPlayer().getWorld().spawn(player.getLocationUtil().getLocation(), Donkey.class);
        horse.setAdult();
        horse.setAgeLock(true);
        horse.setCustomName(player.getMontures().getName());
        horse.setCustomNameVisible(true);
        horse.setTamed(true);
        horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
        horse.setMaxHealth(16);
        horse.setHealth(16);
        horse.addPassenger(player.getPlayer());
        player.getMontures().setMonture(horse);
    }
}
