package fr.ekyla.core.npc.montures;

import fr.ekyla.core.players.EkylaPlayer;
import net.minecraft.server.v1_12_R1.AttributeInstance;
import net.minecraft.server.v1_12_R1.GenericAttributes;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity;
import org.bukkit.entity.Horse;
import org.bukkit.inventory.ItemStack;

public class Horses extends Montures{

    public Horses(EkylaPlayer player, String type, String name, String color, String style, String armor, float speed, float jump, float health) {
        super(player, type, name, color, style, armor, speed, jump, health);
    }

    public static void spawnHorse(EkylaPlayer player) {
        Horse horse = player.getPlayer().getWorld().spawn(player.getLocationUtil().getLocation(), Horse.class);
        horse.setAdult();
        horse.setAgeLock(true);
        horse.setCustomName(player.getMontures().getName());
        horse.setCustomNameVisible(true);
        horse.setColor(Horse.Color.valueOf(player.getMontures().getColor()));
        horse.setStyle(Horse.Style.valueOf(player.getMontures().getStyle()));
        horse.setTamed(true);
        horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
        horse.getInventory().setArmor(new ItemStack(Material.getMaterial(player.getMontures().getArmor())));
        horse.setJumpStrength(player.getMontures().getJump());
        horse.setMaxHealth(player.getMontures().getHealth());
        horse.setHealth(player.getMontures().getHealth());
        AttributeInstance attributes = (((CraftLivingEntity) horse).getHandle()).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
        attributes.setValue(player.getMontures().getSpeed());
        horse.addPassenger(player.getPlayer());
        player.getMontures().setMonture(horse);
    }
}
