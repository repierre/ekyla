package fr.ekyla.core.npc.montures;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.List;

public class Montures {
    private static List<Montures> montureList = new ArrayList<>();
    protected Entity monture;
    private EkylaPlayer player;
    private String type;
    private String name;
    private String color;
    private String style;
    private String armor;
    private float speed;
    private float jump;
    private float health;
    private long lastUse;

    public Montures(EkylaPlayer player, String type, String name, String color, String style, String armor, float speed, float jump, float health) {
        this.player = player;
        this.type = type;
        this.name = name;
        this.color = color;
        this.style = style;
        this.armor = armor;
        this.speed = speed;
        this.jump = jump;
        this.health = health;
        montureList.add(this);
    }

    public static List<Montures> getMontureList() {
        return montureList;
    }

    public void kill() {
        if (monture == null) return;
        monture.remove();
        monture = null;
        lastUse = System.currentTimeMillis();
    }

    public Entity getMonture() {
        return monture;
    }

    public void setMonture(Entity monture) {
        this.monture = monture;
    }

    public void spawn(EkylaPlayer player) {
        switch (getType()) {
            case "Horses":
                Horses.spawnHorse(player);
                break;
            case "Roach":
                Roach.spawnRoach(player);
                break;
            case "Donkeys":
                Donkeys.spawnDonkey(player);
                break;
            case "Mules":
                Mules.spawnMule(player);
                break;
            default:
                System.out.println("Erreur : type de monture invalide.");
                break;
        }
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public void setPlayer(EkylaPlayer player) {
        this.player = player;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getJump() {
        return jump;
    }

    public void setJump(float jump) {
        this.jump = jump;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public boolean canUse() {
        return lastUse == 0 || (((lastUse / 1000) + 30) - System.currentTimeMillis() / 1000) < 0;
    }
}