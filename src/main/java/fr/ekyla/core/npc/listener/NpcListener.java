package fr.ekyla.core.npc.listener;

import fr.ekyla.core.Core;
import fr.ekyla.core.inventories.AbstractInventory;
import fr.ekyla.core.npc.AbstractNpc;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.Random;

public class NpcListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityClick(PlayerInteractEntityEvent event) {
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        Entity entity = event.getRightClicked();
        if (entity instanceof Player) {
            //TODO: Action avec joueurs
            return;
        }
        if (!entity.isCustomNameVisible()) return;
        AbstractInventory inv = AbstractInventory.getInventory(entity.getCustomName().split(" ")[0]);
        if (inv == null) return;
        event.setCancelled(true);
        if (player.isAction()) {
            player.setAction(false);
            return;
        }
        player.setAction(true);
        player.getPlayer().closeInventory();

        if (player.getPlayer().isSneaking()) {
            if (!player.getSkills().canVol()) return;
            Random random = new Random();
            int chance = random.nextInt(100);
            if (chance <= player.getSkills().getVol()) {
                int money = random.nextInt((int) (player.getSkills().getVol() * player.getAmplifier()));
                player.addMoney(money);
                player.sendMessage("§6Petite voix §7»", "§7§oEt voila ! §e" + money + " PO §7§oen plus.");
            } else {
                //TODO: Réputation
                player.sendMessage(entity.getCustomName() + " §7»", "Mais vous avez les mains bien baladeuses dites donc !");
            }
            player.getSkills().setCanVol(false);
            Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> player.getSkills().setCanVol(true), 50 * 20L);
            event.setCancelled(true);
            return;
        }
        inv.open(player);
    }

    @EventHandler
    public void onKill(EntityDeathEvent event) {
        if (!(event.getEntity() instanceof AbstractNpc)) return;
        AbstractNpc.getNpcList().remove(event.getEntity());
    }

}
