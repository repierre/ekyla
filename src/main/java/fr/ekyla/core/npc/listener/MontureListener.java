package fr.ekyla.core.npc.listener;

import fr.ekyla.core.npc.montures.Montures;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class MontureListener implements Listener {

    @EventHandler
    public void onHit(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (!(entity instanceof Montures)) return;
        for (EkylaPlayer player : EkylaPlayer.getEkylaPlayers()) {
            if (player.getMontures() == null) continue;
            Entity monture = player.getMontures().getMonture();
            if (entity == monture) {
                if (((LivingEntity) monture).getHealth() - event.getFinalDamage() < 1 || ((LivingEntity) monture).getHealth() <= 1) {
                    event.setCancelled(true);
                    player.getMontures().kill();
                }
            }
        }
    }
}
