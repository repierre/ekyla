package fr.ekyla.core.utils;

import fr.ekyla.core.Core;
import fr.ekyla.core.npc.AbstractNpc;
import fr.ekyla.core.npc.MerchantNpc;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

/**
 * File <b>JsonWriter</b> located on fr.ekyla.core.utils
 * JsonWriter is a part of ekyla.
 * <p>
 * Copyright (c) 2017 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/03/2018 at 10:07
 */
public class JsonWriter {

    public static void writeToJson(String fileName, String json) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(Core.getInstance().getDataFolder().getAbsolutePath() + File.separator + "npc" + File.separator + fileName));
            writer.write(json);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static AbstractNpc getFromJson(File file) {
        try {
            Scanner scan = new Scanner(file);
            String type = "";
            Location location = null;
            String name = "";
            int gold = 0;
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                if (line.startsWith("type")) {
                    type = line.replaceAll("type", "").replaceAll(": ", "");
                } else if (line.startsWith("location")) {
                    String[] str = line.replaceAll("location", "").replaceAll(": ", "").split(";");
                    location = new Location(Bukkit.getWorld("world"), Double.valueOf(str[0]), Double.valueOf(str[1]), Double.valueOf(str[2]), Float.valueOf(str[3]), Float.valueOf(str[4]));
                } else if (line.startsWith("name")) {
                    name = line.replaceAll("name", "").replaceFirst(": ", "");
                } else if (line.startsWith("gold")) {
                    gold = Integer.valueOf(line.replaceAll("gold", "").replaceFirst(": ", ""));
                }
            }
            try {
                type = type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase();
                Class<?> npcClass = Class.forName("fr.ekyla.core.npc.entity." + type + "Npc");
                Constructor<?> npc = npcClass.getConstructor(String.class, Location.class);
                AbstractNpc abstractNpc = (AbstractNpc) npc.newInstance(name, location);
                abstractNpc.setLocation(location);
                if (abstractNpc instanceof MerchantNpc) {
                    MerchantNpc np = (MerchantNpc) abstractNpc;
                    np.setGold(gold);
                }
                return abstractNpc;
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException | ClassNotFoundException e) {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
