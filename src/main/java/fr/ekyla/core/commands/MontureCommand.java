package fr.ekyla.core.commands;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MontureCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            EkylaPlayer player = EkylaPlayer.getPlayer((Player) sender);
            if (player.hasPermission("monture.spawn")) {
                if (args.length == 0) {
                    if (player.getMontures().canUse() || player.hasPermission("roach.monture")) {
                        if (player.getMontures().getMonture() == null) {
                            player.getMontures().kill();
                            player.getMontures().spawn(player);
                        } else {
                            player.sendMessage("§cVous êtes déja sur votre monture !");
                            return false;
                        }
                    } else {
                        player.sendMessage("§cVotre monture est parti se reposer !");
                        return false;
                    }
                } else {
                    player.sendMessage("§cUtilisation: /monture");
                    return false;
                }
            } else {
                player.sendMessage("§cErreur, vous n'avez pas la permission d'executer cette commande");
                return false;
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Erreur, vous devez être un joueur");
        }
        return false;
    }
}
