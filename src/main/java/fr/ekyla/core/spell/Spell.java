package fr.ekyla.core.spell;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;

import java.util.List;

public abstract class Spell {

    private String name;
    private EkylaPlayer player;
    private int manaCost;
    private int level;
    private ChatColor color;
    private List<String> description;

    Spell(String name, ChatColor color, EkylaPlayer player, int manaCost, int level, List<String> description) {
        this.name = name;
        this.player = player;
        this.manaCost = manaCost;
        this.level = level;
        this.color = color;
        this.description = description;
    }

    public List<String> getDescription() {
        return description;
    }

    public ChatColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public int getManaCost() {
        return manaCost;
    }

    public int getLevel() {
        return level;
    }
}
