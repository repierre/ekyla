package fr.ekyla.core.spell.listener;

import fr.ekyla.core.Core;
import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.spell.Spell;
import fr.ekyla.core.spell.SpellNoTarget;
import fr.ekyla.core.spell.SpellTarget;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.util.Vector;

public class SpellListener implements Listener {

    @EventHandler
    public void spell(AsyncPlayerChatEvent event) {
        EkylaPlayer player = EkylaPlayer.getPlayer(event.getPlayer());
        if (event.getMessage().startsWith("/")) return;
        if (event.getMessage().startsWith("!")) return;
        if (!player.hasSpell(event.getMessage())) return;
        Spell spell = player.getSpell(event.getMessage());
        if (!player.hasMana(spell.getManaCost())) return;
        if (spell instanceof SpellTarget) {
            Location start = player.getPlayer().getEyeLocation();
            Vector increase = start.getDirection();
            for (int i = 0; i <= 30; i++) {
                Location point = start.add(increase);
                for (Entity entities : player.getPlayer().getNearbyEntities(i, 2, i)) {
                    if (entities instanceof Player) {
                        EkylaPlayer target = EkylaPlayer.getPlayer((Player) entities);
                        double x = Math.round(target.getLocationUtil().getLocation().getX());
                        double z = Math.round(target.getLocationUtil().getLocation().getZ());
                        if (x == Math.round(point.getX()) && z == Math.round(point.getZ())) {
                            if (player.removeMana(spell.getManaCost())) {
                                player.sendMessage("§6Sorts §7»", "Vous utilisez " + spell.getName() + " §7sur §a" + target.getRPName() + "§7.");
                                Bukkit.getScheduler().runTask(Core.getInstance(), () -> ((SpellTarget) spell).use(target));
                                return;
                            }
                        }
                    }
                }
            }
            player.sendMessage("§6Sorts §7»", "§cHum... Aucune cible en vue !");
        } else {
            if (player.removeMana(spell.getManaCost())) {
                player.sendMessage("§6Sorts §7»", "Vous utilisez " + spell.getName() + "§7.");
                Bukkit.getScheduler().runTask(Core.getInstance(), ((SpellNoTarget) spell)::use);
            }
        }
    }

}
