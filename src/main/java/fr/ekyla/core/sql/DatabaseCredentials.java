package fr.ekyla.core.sql;

public class DatabaseCredentials {

    private String host;
    private String user;
    private String password;
    private String databaseName;
    private int port;

    public DatabaseCredentials(String host, String user, String password, String databaseName, int port) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.databaseName = databaseName;
        this.port = port;
    }

    public String toURI() {
        return "jdbc:mysql://" +
                host +
                ":" +
                port +
                "/" +
                databaseName +
                "?useSSL=false";
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
